/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Autoservis;

import udalostnaSimulacia.SimulacneJadro;

/**
 *
 * @author Robo
 */
public class Udalost12_ZaciatokPrevzatiaOpravenehoAuta extends AutoservisUdalost {

    private AutoservisSimulacneJadro jadroAutoservis;

    public Udalost12_ZaciatokPrevzatiaOpravenehoAuta(double casVyskytu, SimulacneJadro simJadro, AutoservisZakaznik zakaznik) {
        super(casVyskytu, simJadro, zakaznik);
        this.jadroAutoservis = (AutoservisSimulacneJadro) simJadro;
    }

    @Override
    public void execute() {
        /**
         *  atributy------------------------------------------------------------
         */
        
        /**
         *  povinne udalosti----------------------------------------------------
         */
        double casKoncaPrevzatiaOpravenehoAuta = this.getCasVyskytu() + jadroAutoservis.dalsiCasDlzkyPrevzatiaOpravenehoAuta() ;
        jadroAutoservis.pridajUdalost(new Udalost13_KoniecPrevzatiaOpravenehoAuta(casKoncaPrevzatiaOpravenehoAuta, jadroAutoservis, this.getZakaznik()));
        /**
         *  volitelne udalosti--------------------------------------------------
         */
        
        /**
         *  statistika----------------------------------------------------------
         */
    }
    
}
