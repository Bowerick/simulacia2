/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Autoservis;

import generator.DiskretneRovnomerne;
import generator.EmpirickeDiskretne;
import generator.Exponencialne;
import generator.SpojiteRovnomerne;
import generator.Trojuholnikove;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Random;
import udalostnaSimulacia.SimulacneJadro;

/**
 *
 * @author Robo
 */
public class AutoservisSimulacneJadro extends SimulacneJadro {

    private Random generatorNasad;
    private Exponencialne generatorPrichodov;
    private SpojiteRovnomerne generatorDlzkyPrevzatiaObjednavky;
    private SpojiteRovnomerne generatorDlzkyPrevzatiaAutaOdZakaznika;
    private Trojuholnikove generatorDlzkyPreparkovania;
    private Trojuholnikove generatorDlzkyPreparkovania2;
    private EmpirickeDiskretne generatorPoctuOprav;
    private EmpirickeDiskretne generatorTypuJednejOpravy;
    private EmpirickeDiskretne generatorTypuStredneTazkejOpravy;
    private DiskretneRovnomerne generatorDlzkyJednoduchejOpravy;
    private DiskretneRovnomerne generatorDlzkyStredneTazkejOpravy1;
    private DiskretneRovnomerne generatorDlzkyStredneTazkejOpravy2;
    private DiskretneRovnomerne generatorDlzkyStredneTazkejOpravy3;
    private DiskretneRovnomerne generatorDlzkyZlozitejOpravy;
    private SpojiteRovnomerne generatorDlzkyPrevzatiaOpravenehoAuta;
    
    private LinkedList<AutoservisZakaznik> frontAutCakajucichNaOpravu;
    private LinkedList<AutoservisZakaznik> frontZakaznikovPriPrichode;
    private LinkedList<AutoservisZakaznik> frontAutCakajucichNaPreparkovanie;
    
    private int pocetPracovnikovTypu1;
    private int pocetPracovnikovTypu2;
    private boolean[] obsluha1;
    private boolean[] obsluha2;
    
    private AutoservisStatistika statistika;
    private int celkovyPocetReplikacii;
    private boolean zahriate;
    private double periodaAnimacie;
    
    public AutoservisSimulacneJadro(int pocetReplikacii, AutoservisStatistika statistika, int pocetPracovnikovTypu1, int pocetPracovnikovTypu2) throws Exception {
        super(pocetReplikacii);
        this.celkovyPocetReplikacii = pocetReplikacii;
        this.statistika = statistika;
        this.zahriate = false;
        this.periodaAnimacie = 1.0;
        
        this.generatorNasad = new Random();
        this.generatorPrichodov = new Exponencialne(generatorNasad.nextLong(),5.0*60.0);
        this.generatorDlzkyPrevzatiaObjednavky = new SpojiteRovnomerne(generatorNasad.nextLong(), 70.0, 310.0);
        this.generatorDlzkyPrevzatiaAutaOdZakaznika = new SpojiteRovnomerne(generatorNasad.nextLong(), 80.0, 160.0);
        this.generatorDlzkyPreparkovania = new Trojuholnikove(generatorNasad.nextLong(), 120.0, 540.0, 240.0);
        
        this.generatorDlzkyPreparkovania2 = new Trojuholnikove(generatorNasad.nextLong(), 120.0, 540.0, 240.0);
        
        this.generatorPoctuOprav = new EmpirickeDiskretne(generatorNasad.nextLong(), new double[]{0.4,0.15,0.14,0.12,0.1,0.09});
        this.generatorTypuJednejOpravy = new EmpirickeDiskretne(generatorNasad.nextLong(), new double[]{0.7,0.2,0.1});
        
        this.generatorDlzkyJednoduchejOpravy = new DiskretneRovnomerne(generatorNasad.nextLong(), 2, 20);//a
        
        this.generatorTypuStredneTazkejOpravy = new EmpirickeDiskretne(generatorNasad.nextLong(), new double[]{0.1,0.6,0.3});
        this.generatorDlzkyStredneTazkejOpravy1 = new DiskretneRovnomerne(generatorNasad.nextLong(), 10, 40);
        this.generatorDlzkyStredneTazkejOpravy2 = new DiskretneRovnomerne(generatorNasad.nextLong(), 41, 61);
        this.generatorDlzkyStredneTazkejOpravy3= new DiskretneRovnomerne(generatorNasad.nextLong(), 62, 100);
        
        this.generatorDlzkyZlozitejOpravy = new DiskretneRovnomerne(generatorNasad.nextLong(), 120, 260);
        
        this.generatorDlzkyPrevzatiaOpravenehoAuta = new SpojiteRovnomerne(generatorNasad.nextLong(), 123.0, 257.0);
        
        this.pocetPracovnikovTypu1 = pocetPracovnikovTypu1;
        this.pocetPracovnikovTypu2 = pocetPracovnikovTypu2;
        
        this.frontZakaznikovPriPrichode = new LinkedList();
        this.frontAutCakajucichNaOpravu = new LinkedList();
        this.frontAutCakajucichNaPreparkovanie = new LinkedList();
        
        this.obsluha1 = new boolean[pocetPracovnikovTypu1];
        Arrays.fill(obsluha1, true);
        this.obsluha2 = new boolean[pocetPracovnikovTypu2];
        Arrays.fill(obsluha2, true);
        this.statistika.inicializacia(pocetPracovnikovTypu1,pocetPracovnikovTypu2);
    }

    @Override
    public void predReplikaciou() {
        vynulujCasovuOs();
        setSimulacnyCas(0.0);
        
        pridajUdalost(getStartovaciaUdalost());
        pridajUdalost(new Udalost_NovyDen(8.0 * 60.0 * 60.0, this, null));
        pridajUdalost(new Udalost_Zahriatie(30.0 * 8.0 * 60.0 * 60.0, this, null));
        
        if(isAnimacia()){
            pridajUdalost(new Udalost_Animacia(getPeriodaAnimacie(), this, null));
            setZacniAnimaciu(false);
        }
        vynulujRadZakaznikovPriPrichode();
        vynulujRadAutCakajucichNaOpravu();
        vynulujRadAutCakajucichNaPreparkovanieZDielne();
        
        Arrays.fill(obsluha1, true);
        Arrays.fill(obsluha2, true);
        
        this.getStatistika().vynuluj();
        this.setZahriate(false);
    }
    
    @Override
    public void poReplikacii() {
        this.statistika.aktualizujAktualnuReplikaciu();
        //statistika 1
        this.statistika.pridajPriemerCasuCakaniaNaOpravu1(this.statistika.getSumaCasovCakaniaNaOpravu1()/(double)this.statistika.getPocetZakaznikovPoOdovzdaniOpravenehoAuta1());
        this.statistika.setStatistika1(this.statistika.getSumaPriemerovCasovCakaniaNaOpravu1()/(double)this.statistika.getAktualnaReplikacia());
        this.statistika.setIs1(this.statistika.vypocitajIntervalSpolahlivosti(this.statistika.getSumaPriemerovCasovCakaniaNaOpravuNaDruhu1(),this.statistika.getSumaPriemerovCasovCakaniaNaOpravu1()));
        
        //sem 3 kontrola
        this.statistika.pridajPriemerCasuCakaniaNaOpravu1111(this.statistika.getSumaCasovCakaniaNaOpravu1111()/(double)this.statistika.getPocetZakaznikovPoSkonceniKontroly1111());
        this.statistika.setStatistika1111(this.statistika.getSumaPriemerovCasovCakaniaNaOpravu1111()/(double)this.statistika.getAktualnaReplikacia());
        //this.statistika.setIs1(this.statistika.vypocitajIntervalSpolahlivosti(this.statistika.getSumaPriemerovCasovCakaniaNaOpravuNaDruhu1(),this.statistika.getSumaPriemerovCasovCakaniaNaOpravu1()));
        

        //statistika 2
        this.statistika.pridajPriemerCasuCakaniaVRadeNaZadanieObjednavky2(this.statistika.getSumaCasovCakaniaVRadeNaZadanieObjednavky2()/(double)this.statistika.getPocetZakaznikovPoSkonceniZadaniaObjednavky2());
        this.statistika.setStatistika2(this.statistika.getSumaPriemerovCasovCakaniaVRadeNaZadanieObjednavky2()/(double)this.statistika.getAktualnaReplikacia());
        this.statistika.setIs2(this.statistika.vypocitajIntervalSpolahlivosti(this.statistika.getSumaPriemerovCasovCakaniaVRadeNaZadanieObjednavkyNaDruhu2(),this.statistika.getSumaPriemerovCasovCakaniaVRadeNaZadanieObjednavky2()));
        //statistika 4 pocet zakaznikov po odovzdani opraveneho auta = pocet zakaznikov, ktori presli systemom
        this.statistika.pridajPriemerCasuStravenehoZakaznikomVServise4(this.statistika.getSumaCasovStravenychZakaznikomVServise4()/(double)this.statistika.getPocetZakaznikovPoOdovzdaniOpravenehoAuta1());
        this.statistika.setStatistika4(this.statistika.getSumaPriemerovCasovStravenychZakaznikomVServise4()/(double)this.statistika.getAktualnaReplikacia());
        //statistika 5
        this.statistika.pridajPriemerPoctuVolnychPracovnikov1(this.statistika.getSumaPocetVolnychPracovnikov1()/(double)((90.0-30.0)*8.0*60.0*60.0));
        
        //statistika 6
        this.statistika.pridajPriemerPoctuVolnychPracovnikov2(this.statistika.getSumaPocetVolnychPracovnikov2()/(double)((90.0-30.0)*8.0*60.0*60.0));
        //statistika 7
        this.statistika.pridajPriemerPoctuLudiVRadePrichodov7(this.statistika.getSumaPocetLudiVRadePrichodov7()/(double)((90.0-30.0)*8.0*60.0*60.0));
        this.statistika.setStatistika7(this.statistika.getSumaPocetLudiVRadePrichodov7()/(double)((90.0-30.0)*8.0*60.0*60.0));
        /*
           System.out.println(this.statistika.getAktualnaReplikacia());
            System.out.println("statistika  1: "+this.statistika.getSumaPriemerovCasovCakaniaNaOpravu1()/(double)this.statistika.getAktualnaReplikacia());
            double[] is1 = this.statistika.vypocitajIntervalSpolahlivosti(this.statistika.getSumaPriemerovCasovCakaniaNaOpravuNaDruhu1(),this.statistika.getSumaPriemerovCasovCakaniaNaOpravu1());
            System.out.println("Interval spolahlivosti statistika1: <"+is1[0]+", "+is1[1]+">");
            */
            if((this.statistika.getSumaCasovCakaniaVRadeNaZadanieObjednavky2()/(double)this.statistika.getPocetZakaznikovPoSkonceniZadaniaObjednavky2()) > 200)
             System.out.println("\n\n wtf: "+this.statistika.getSumaCasovCakaniaVRadeNaZadanieObjednavky2()/(double)this.statistika.getPocetZakaznikovPoSkonceniZadaniaObjednavky2());
            /*System.out.println("statistika  2: "+this.statistika.getSumaPriemerovCasovCakaniaVRadeNaZadanieObjednavky2()/(double)this.statistika.getAktualnaReplikacia());      
            double[] is2 = this.statistika.vypocitajIntervalSpolahlivosti(this.statistika.getSumaPriemerovCasovCakaniaVRadeNaZadanieObjednavkyNaDruhu2(),this.statistika.getSumaPriemerovCasovCakaniaVRadeNaZadanieObjednavky2());
            System.out.println("Interval spolahlivosti statistika2: <"+is2[0]+", "+is2[1]+">");
            
            System.out.println("statistika  3: ----------");
            System.out.println("statistika  4: "+this.statistika.getSumaPriemerovCasovStravenychZakaznikomVServise4()/(double)this.statistika.getAktualnaReplikacia());
            System.out.println("statistika  5: "+this.statistika.getSumaPriemerovPoctuVolnychPracovnikov1()/(double)this.statistika.getAktualnaReplikacia());
            System.out.println("statistika  6: "+this.statistika.getSumaPriemerovPoctuVolnychPracovnikov2()/(double)this.statistika.getAktualnaReplikacia());
            System.out.println("statistika  7: "+this.statistika.getSumaPriemerovPoctuLudiVRadePrichodov7()/(double)this.statistika.getAktualnaReplikacia());
            System.out.println("pocet zakaznikov po odovzdani opraveneho auta 1: "+this.statistika.getPocetZakaznikovPoOdovzdaniOpravenehoAuta1());
            System.out.println("pocet zakaznikov po zadani objednavky 2: "+this.statistika.getPocetZakaznikovPoSkonceniZadaniaObjednavky2());
        */
        
    }

    @Override
    public void zacniAnimaciu() {
        pridajUdalost(new Udalost_Animacia(this.getSimulacnyCas(), this, null));
        setZacniAnimaciu(false);
    }
    
    public double dalsiCasPrichoduZakaznika() {
        return this.generatorPrichodov.nextDouble();
    }
    
    public double dalsiCasDlzkyPrevzatieObjednavky() {
        return this.generatorDlzkyPrevzatiaObjednavky.nextDouble();
    }
    
    public double dalsiCasDlzkyPrevzatiaAutaOdZakaznika() {
        return this.generatorDlzkyPrevzatiaAutaOdZakaznika.nextDouble();
    }
    
    public double dalsiCasDlzkyPreparkovaniaAuta() {
        return this.generatorDlzkyPreparkovania.nextDouble();
    }
    
    public double dalsiCasDlzkyPreparkovaniaAuta2() {
        return this.generatorDlzkyPreparkovania2.nextDouble();
    }
    
    /**
     *
     * @return pocet pozadovanych oprav: 1-6
     */
    public int dalsiPocetOprav() {
        return this.generatorPoctuOprav.nextInt();
    }
    
    /**
     *
     * @return typ opravy. 1, 2, 3 - jednoduca, stredne tazka, zlozita
     */
    public int dalsiTypOpravy() {
        return this.generatorTypuJednejOpravy.nextInt();
    }
    
    public int dalsiCasDlzkyJednoduchejOpravy() {
        return this.generatorDlzkyJednoduchejOpravy.nextInt() * 60;
    }
    
    public int dalsiTypStredneTazkejOpravy() {
        return this.generatorTypuStredneTazkejOpravy.nextInt() ;
    }
    
    public int dalsiCasDlzkyStredneTazkejOpravy1() {
        return this.generatorDlzkyStredneTazkejOpravy1.nextInt() * 60;
    }
    
    public int dalsiCasDlzkyStredneTazkejOpravy2() {
        return this.generatorDlzkyStredneTazkejOpravy2.nextInt() * 60;
    }
    
    public int dalsiCasDlzkyStredneTazkejOpravy3() {
        return this.generatorDlzkyStredneTazkejOpravy3.nextInt() * 60;
    }
    
    public int dalsiCasDlzkyZlozitejOpravy() {
        return this.generatorDlzkyZlozitejOpravy.nextInt() * 60;
    }
    
    public double dalsiCasDlzkyPrevzatiaOpravenehoAuta() {
        return this.generatorDlzkyPrevzatiaOpravenehoAuta.nextDouble();
    }
    
    //FRONT ZAKAZNIKOV PRI PRICHODE
    
    public boolean pridajDoFrontuZakaznikovPriPrichode(AutoservisZakaznik zakaznik) {
        return this.frontZakaznikovPriPrichode.add(zakaznik);
    }
    
    public AutoservisZakaznik odoberZFrontuZakaznikovPriPrichode() {
        return this.frontZakaznikovPriPrichode.poll();
    }
    
    public boolean jeFrontZakaznikovPriPrichodePrazdny() {
        return this.frontZakaznikovPriPrichode.isEmpty();
    }
    
    void vynulujRadZakaznikovPriPrichode() {
        this.frontZakaznikovPriPrichode.clear();
    }

    //FRONT AUT CAKAJUCICH NA OPRAVU
    
    public boolean pridajDoFrontuAutCakajucichNaOpravu(AutoservisZakaznik zakaznik) {
        return this.frontAutCakajucichNaOpravu.add(zakaznik);
    }
    
    public AutoservisZakaznik odoberZFrontuAutCakajucichNaOpravu() {
        return this.frontAutCakajucichNaOpravu.poll();
    }
    
    public boolean jeFrontAutCakajucichNaOpravuPrazdny() {
        return this.frontAutCakajucichNaOpravu.isEmpty();
    }

    void vynulujRadAutCakajucichNaOpravu() {
        this.frontAutCakajucichNaOpravu.clear();
    }
    
    //FRONT AUT CAKAJUCICH NA PREPARKOVANIE
    
    public boolean pridajDoFrontuAutCakajuchNaPreparkovanieZDielne(AutoservisZakaznik zakaznik) {
        return this.frontAutCakajucichNaPreparkovanie.add(zakaznik);
    }
    
    public AutoservisZakaznik odoberZFrontuAutCakajuchNaPreparkovanieZDielne() {
        return this.frontAutCakajucichNaPreparkovanie.poll();
    }
    
    public boolean jeFrontAutCakajucichNaPreparkovanieZDielnePrazdny() {
        return this.frontAutCakajucichNaPreparkovanie.isEmpty();
    }

    void vynulujRadAutCakajucichNaPreparkovanieZDielne() {
        this.frontAutCakajucichNaPreparkovanie.clear();
    }

    public int getVolnyPracovnik1() {
        int idx = -1;
        for(int i=0;i<obsluha1.length;i++) {
            if(obsluha1[i]) {
                idx = i;
                break;
            }
        }
        return idx;
    }

    public int getVolnyPracovnik2() {
        int idx = -1;
        for(int i=0;i<obsluha2.length;i++) {
            if(obsluha2[i]) {
                idx = i;
                break;
            }
        }
        return idx;
    }
    
    public int getPocetVolnychPracovnikov1() {
        int suma = 0;
        for(int i=0;i<obsluha1.length;i++) {
            if(obsluha1[i])
                suma++;
        }
        return suma;
    }
    
    public int getPocetVolnychPracovnikov2() {
        int suma = 0;
        for(int i=0;i<obsluha2.length;i++) {
            if(obsluha2[i])
                suma++;
        }
        return suma;
    }
    
    public void obsadPracovnikaTypu1(int index) {
        this.obsluha1[index] = false;
    }
    
    public void uvolniPracovnikaTypu1(int index) {
        this.obsluha1[index] = true;
    }
    
    public void obsadPracovnikaTypu2(int index) {
        this.obsluha2[index] = false;
    }
    
    public void uvolniPracovnikaTypu2(int index) {
        this.obsluha2[index] = true;
    }

    /**
     * @return the statistika
     */
    public AutoservisStatistika getStatistika() {
        return statistika;
    }

    /**
     * @return the zahriate
     */
    public boolean isZahriate() {
        return zahriate;
    }

    /**
     * @param zahriate the zahriate to set
     */
    public void setZahriate(boolean zahriate) {
        this.zahriate = zahriate;
    }

    /**
     * @return the pocetPracovnikovTypu1
     */
    public int getPocetPracovnikovTypu1() {
        return pocetPracovnikovTypu1;
    }

    /**
     * @return the pocetPracovnikovTypu2
     */
    public int getPocetPracovnikovTypu2() {
        return pocetPracovnikovTypu2;
    }

    /**
     * @return the periodaAnimacie
     */
    public double getPeriodaAnimacie() {
        return periodaAnimacie;
    }

    /**
     * @param periodaAnimacie the periodaAnimacie to set
     */
    public void setPeriodaAnimacie(double periodaAnimacie) {
        this.periodaAnimacie = periodaAnimacie;
    }
    
    public int getDlzkaFrontuPrichodovZakaznikov() {
        return this.frontZakaznikovPriPrichode.size();
    }
    
    public int getDlzkaFrontuAutCakajucichNaOpravu() {
        return this.frontAutCakajucichNaOpravu.size();
    }
    
    public int getDlzkaFrontuAutCakajucichNaPreparkovanie() {
        return this.frontAutCakajucichNaPreparkovanie.size();
    }
    
    public String getVolniPracovnic1() {
        String res = "";
        for(int i=0;i<obsluha1.length;i++) {
            if(obsluha1[i])
                res += (i+1)+", ";
        }
        return res;
    }
    
    public String getVolniPracovnici2() {
        String res = "";
        for(int i=0;i<obsluha2.length;i++) {
            if(obsluha2[i])
                res += (i+1)+", ";
        }
        return res;
    }
    
}
