/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Autoservis;

import udalostnaSimulacia.SimulacneJadro;
import udalostnaSimulacia.Udalost;

/**
 *
 * @author Robo
 */
public abstract class AutoservisUdalost extends Udalost {
    
    private AutoservisZakaznik zakaznik;
    
    public AutoservisUdalost(double casVyskytu, SimulacneJadro simJadro, AutoservisZakaznik zakaznik) {
        super(casVyskytu, simJadro);
        this.zakaznik = zakaznik;
    }

    /**
     * @return the zakaznik
     */
    public AutoservisZakaznik getZakaznik() {
        return zakaznik;
    }
    
}
