/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Autoservis;

import udalostnaSimulacia.IDelegat;

/**
 *
 * @author Robo
 */
public class App implements Runnable {
    
    private AutoservisSimulacneJadro servis;
    private int pocetPracovnikovTypu1;
    private int pocetPracovnikovTypu2;
    private int pocetReplikacii;
    private IDelegat delegat;
    private double animacnaPerioda;
    private boolean zavislosti;
    private boolean zavislosti2;
    private boolean excel;
    private boolean animacia;
    
    public App() {
    }

    @Override
    public void run() {
        AutoservisStatistika statistika = new AutoservisStatistika();
        try {
            //servis = new AutoservisSimulacneJadro(pocetReplikacii, statistika, 100, 100);
            servis = new AutoservisSimulacneJadro(pocetReplikacii, statistika, pocetPracovnikovTypu1, pocetPracovnikovTypu2);
            this.servis.setExcel(false);
            //servis = new AutoservisSimulacneJadro(pocetReplikacii, statistika, 5, 50);
            AutoservisZakaznik z = new AutoservisZakaznik();
            Udalost01_PrichodZakaznika u = new Udalost01_PrichodZakaznika(0.0, getServis(), z);
            servis.setPeriodaAnimacie(animacnaPerioda);
            servis.setStartovaciaUdalost(u);
            servis.setPauza(false);
            servis.setKoniec(false);
            
            if(isAnimacia()) {
                servis.setAnimacia(true);
                servis.setZacniAnimaciu(true);
            } else {
                
                servis.setAnimacia(false);
                servis.setZacniAnimaciu(false);
            }
            //servis.setZavislosti(false);
            /*
            servis.setAnimacia(false);
            servis.setZacniAnimaciu(false);
            */
            servis.setZavislosti(zavislosti);
            servis.setZavislosti2(zavislosti2);
            servis.setDelegat(delegat);
            servis.simuluj(90.0*8.0*60.0*60.0);//2592000//7776000//1000000000
            //servis.setAnimacia(false);
            //servis.setZacniAnimaciu(false);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println(ex);
        }
    }
    
    public void setDelegat(IDelegat delegat) {
        this.delegat = delegat;
    }

    void pozastav() {
        getServis().setPauza(true);
    }
    
    void pokracuj() {
        getServis().setPauza(false);
    }
    
    void spustAnimaciu(double perioda) {
        servis.setPeriodaAnimacie(perioda);
        setAnimacia(true);
        getServis().setAnimacia(true);
        getServis().setZacniAnimaciu(true);
    }
    
    void nastavPerioduAnimacie(double perioda) {
        servis.setPeriodaAnimacie(perioda);
    }
    
    void zastavAnimaciu() {
        setAnimacia(false);
        getServis().setAnimacia(false);
        getServis().setZacniAnimaciu(false);
    }
    
    public void ukonci() {
        servis.setKoniec(true);
    }

    /**
     * @return the servis
     */
    public AutoservisSimulacneJadro getServis() {
        return servis;
    }

    /**
     * @return the pocetPracovnikovTypu1
     */
    public int getPocetPracovnikovTypu1() {
        return pocetPracovnikovTypu1;
    }

    /**
     * @param pocetPracovnikovTypu1 the pocetPracovnikovTypu1 to set
     */
    public void setPocetPracovnikovTypu1(int pocetPracovnikovTypu1) {
        this.pocetPracovnikovTypu1 = pocetPracovnikovTypu1;
    }

    /**
     * @return the pocetPracovnikovTypu2
     */
    public int getPocetPracovnikovTypu2() {
        return pocetPracovnikovTypu2;
    }

    /**
     * @param pocetPracovnikovTypu2 the pocetPracovnikovTypu2 to set
     */
    public void setPocetPracovnikovTypu2(int pocetPracovnikovTypu2) {
        this.pocetPracovnikovTypu2 = pocetPracovnikovTypu2;
    }

    /**
     * @return the pocetReplikacii
     */
    public int getPocetReplikacii() {
        return pocetReplikacii;
    }

    /**
     * @param pocetReplikacii the pocetReplikacii to set
     */
    public void setPocetReplikacii(int pocetReplikacii) {
        this.pocetReplikacii = pocetReplikacii;
    }
    
    public double getAktualnyCasSimulacie() {
        return servis.getSimulacnyCas();
    }
    
    public int getAktualnaReplikacia() {
        return servis.getStatistika().getAktualnaReplikacia();
    }

    /**
     * @param animacnaPerioda the animacnaPerioda to set
     */
    public void setAnimacnaPerioda(double animacnaPerioda) {
        this.animacnaPerioda = animacnaPerioda;
    }

    /**
     * @return the zavislosti
     */
    public boolean isZavislosti() {
        return zavislosti;
    }

    /**
     * @param zavislosti the zavislosti to set
     */
    public void setZavislosti(boolean zavislosti) {
        this.zavislosti = zavislosti;
    }

    /**
     * @return the zavislosti2
     */
    public boolean isZavislosti2() {
        return zavislosti2;
    }

    /**
     * @param zavislosti2 the zavislosti2 to set
     */
    public void setZavislosti2(boolean zavislosti2) {
        this.zavislosti2 = zavislosti2;
    }
    
    public void setExcel(boolean hodnota) {
        this.excel = hodnota;
    }
    
    public boolean isExcel() {
        return this.servis.isExcel();
        
    }
    
    public boolean isAnimacia() {
        return animacia;
    }
    
    public void setAnimacia(boolean h) {
        this.animacia = h;
    }
}
