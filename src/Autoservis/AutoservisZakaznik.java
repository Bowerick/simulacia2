/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Autoservis;

/**
 *
 * @author Robo
 */
public class AutoservisZakaznik {
        
    private int idxPracovnikaTypu1Objednavka;
    private int idxPracovnikaTypu1Preparkovanie;
    private int idxPracovnikaTypu2;
    
    private int pocetOprav;
    private int[] typyOprav;
    private int[] dlzkyOprav;
    
    private int celkovyCasOpravy;
    
    //statistika 2
    private double casPrichodu;
    
    //statistika 1
    private double casZaciatkuCakaniaNaOpravuPoOdovzdaniAuta1;
    
    private int idAnimacie;
    
    public AutoservisZakaznik() {
        
    }

    public void inicializujPolia(int pocetOprav) {
        this.pocetOprav = pocetOprav;
        this.typyOprav = new int[pocetOprav];
        this.dlzkyOprav = new int[pocetOprav];
    }

    void setTypOpravy(int i, int typOpravy) {
        typyOprav[i] = typOpravy;
    }

    void setTypOpravy2(int i, int typStredneTazkejOpravy) {
        typyOprav[i] = typyOprav[i]*10 + typStredneTazkejOpravy;
    }

    void setDlzkaOpravy(int i, int dlzkaOpravy) {
        dlzkyOprav[i] = dlzkaOpravy;
    }

    /**
     * @return the celkovyCasOpravy
     */
    public int getCelkovyCasOpravy() {
        return celkovyCasOpravy;
    }

    /**
     * @param celkovyCasOpravy the celkovyCasOpravy to set
     */
    public void setCelkovyCasOpravy(int celkovyCasOpravy) {
        this.celkovyCasOpravy = celkovyCasOpravy;
    }

    /**
     * @return the casPrichodu
     */
    public double getCasPrichodu() {
        return casPrichodu;
    }

    /**
     * @param casPrichodu the casPrichodu to set
     */
    public void setCasPrichodu(double casPrichodu) {
        this.casPrichodu = casPrichodu;
    }

    /**
     * @return the idxPracovnikaTypu1Objednavka
     */
    public int getIdxPracovnikaTypu1Objednavka() {
        return idxPracovnikaTypu1Objednavka;
    }

    /**
     * @param idxPracovnikaTypu1Objednavka the idxPracovnikaTypu1Objednavka to set
     */
    public void setIdxPracovnikaTypu1Objednavka(int idxPracovnikaTypu1Objednavka) {
        this.idxPracovnikaTypu1Objednavka = idxPracovnikaTypu1Objednavka;
    }

    /**
     * @return the idxPracovnikaTypu1Preparkovanie
     */
    public int getIdxPracovnikaTypu1Preparkovanie() {
        return idxPracovnikaTypu1Preparkovanie;
    }

    /**
     * @param idxPracovnikaTypu1Preparkovanie the idxPracovnikaTypu1Preparkovanie to set
     */
    public void setIdxPracovnikaTypu1Preparkovanie(int idxPracovnikaTypu1Preparkovanie) {
        this.idxPracovnikaTypu1Preparkovanie = idxPracovnikaTypu1Preparkovanie;
    }

    /**
     * @return the idxPracovnikaTypu2
     */
    public int getIdxPracovnikaTypu2() {
        return idxPracovnikaTypu2;
    }

    /**
     * @param idxPracovnikaTypu2 the idxPracovnikaTypu2 to set
     */
    public void setIdxPracovnikaTypu2(int idxPracovnikaTypu2) {
        this.idxPracovnikaTypu2 = idxPracovnikaTypu2;
    }

    /**
     * @return the casZaciatkuCakaniaNaOpravuPoPrevzatiAuta
     */
    public double getCasZaciatkuCakaniaNaOpravuPoOdovzdaniAuta1() {
        return casZaciatkuCakaniaNaOpravuPoOdovzdaniAuta1;
    }

    /**
     * @param casZaciatkuCakaniaNaOpravuPoPrevzatiAuta the casZaciatkuCakaniaNaOpravuPoPrevzatiAuta to set
     */
    public void setCasZaciatkuCakaniaNaOpravuPoOdovzdaniAuta1(double casZaciatkuCakaniaNaOpravuPoPrevzatiAuta) {
        this.casZaciatkuCakaniaNaOpravuPoOdovzdaniAuta1 = casZaciatkuCakaniaNaOpravuPoPrevzatiAuta;
    }

    /**
     * @return the idAnimacie
     */
    public int getIdAnimacie() {
        return idAnimacie;
    }

    /**
     * @param idAnimacie the idAnimacie to set
     */
    public void setIdAnimacie(int idAnimacie) {
        this.idAnimacie = idAnimacie;
    }
}
