/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Autoservis;

import udalostnaSimulacia.SimulacneJadro;

/**
 *
 * @author Robo
 */
public class Udalost10_ZaciatokPreparkovaniaAutaZDielne extends AutoservisUdalost {

    private AutoservisSimulacneJadro jadroAutoservis;

    public Udalost10_ZaciatokPreparkovaniaAutaZDielne(double casVyskytu, SimulacneJadro simJadro, AutoservisZakaznik zakaznik) {
        super(casVyskytu, simJadro, zakaznik);
        this.jadroAutoservis = (AutoservisSimulacneJadro) simJadro;
    }

    @Override
    public void execute() {
        /**
         *  atributy------------------------------------------------------------
         */
        
        /**
         *  povinne udalosti----------------------------------------------------
         */
        double casKoncaPreparkovaniaAutaZDielne = this.getCasVyskytu() + jadroAutoservis.dalsiCasDlzkyPreparkovaniaAuta2(); 
        jadroAutoservis.pridajUdalost(new Udalost11_KoniecPreparkovaniaAutaZDielne(casKoncaPreparkovaniaAutaZDielne, jadroAutoservis, this.getZakaznik()));
        
        /**
         *  volitelne udalosti--------------------------------------------------
         */
        
        /**
         *  statistika----------------------------------------------------------
         */
    }
    
}
