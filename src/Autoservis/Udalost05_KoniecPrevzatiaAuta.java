/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Autoservis;

import udalostnaSimulacia.SimulacneJadro;

/**
 *
 * @author Robo
 */
public class Udalost05_KoniecPrevzatiaAuta extends AutoservisUdalost {

    private AutoservisSimulacneJadro jadroAutoservis;

    public Udalost05_KoniecPrevzatiaAuta(double casVyskytu, SimulacneJadro simJadro, AutoservisZakaznik zakaznik) {
        super(casVyskytu, simJadro, zakaznik);
        this.jadroAutoservis = (AutoservisSimulacneJadro) simJadro;
    }

    @Override
    public void execute() {
        
        /**
         *  atributy------------------------------------------------------------
         */
        //priemerný čas strávený zákazníkom čakaním na opravu - zaciatok (statistika 1)
        this.getZakaznik().setCasZaciatkuCakaniaNaOpravuPoOdovzdaniAuta1(this.getCasVyskytu());
        
        /**
         *  povinne udalosti----------------------------------------------------
         */
        jadroAutoservis.pridajUdalost(new Udalost06_ZaciatokPreparkovaniaAutaDoDielne(this.getCasVyskytu(), jadroAutoservis, this.getZakaznik()));
        
        /**
         *  volitelne udalosti--------------------------------------------------
         */
        
        /**
         *  statistika----------------------------------------------------------
         */
    }
    
}
