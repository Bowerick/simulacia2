/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Autoservis;

import java.util.Arrays;

/**
 *
 * @author Robo
 */
public class AutoservisStatistika {
    
    /**
     * statistika 1 - 
     * priemerný čas strávený zákazníkom čakaním na opravu
     *(čas začína plynúť okamihom ukončenia prevzatia auta do servisu a končí prevzatím opraveného auta) 
     */
    private double sumaCasovCakaniaNaOpravu1;
    private double sumaCasovCakaniaNaOpravu1111;
    private double sumaPriemerovCasovCakaniaNaOpravu1;
    private double sumaPriemerovCasovCakaniaNaOpravu1111;
    private double sumaPriemerovCasovCakaniaNaOpravuNaDruhu1;
    private double sumaPriemerovCasovCakaniaNaOpravuNaDruhu1111;
    private double statistika1;
    private double statistika1111;
    private double[] is1;
    
    /**
     * statistika 2 - priemerny cas cakania v rade na zadanie objednavky
     */
    private double sumaCasovCakaniaVRadeNaZadanieObjednavky2;
    private double sumaPriemerovCasovCakaniaVRadeNaZadanieObjednavky2;
    private double sumaPriemerovCasovCakaniaVRadeNaZadanieObjednavkyNaDruhu2;
    private double statistika2;
    private double[] is2;
    
    /**
     * statistika 4 - priemerny cas straveny zakaznikov v servise
     */
    private double sumaCasovStravenychZakaznikomVServise4;
    private double sumaPriemerovCasovStravenychZakaznikomVServise4;
    private double statistika4;
    
    /**
     * statistika 5 - priemerny pocet volnych pracovnikov typu 1
     */
    private double casPoslednejZmenyObsluha1;
    private double sumaPocetVolnychPracovnikov1;
    private double sumaPriemerovPoctuVolnychPracovnikov1;
    
    /**
     * statistika 6 - priemerny pocet volnych pracovnikov typu 2
     */
    private double casPoslednejZmenyObsluha2;
    private double sumaPocetVolnychPracovnikov2;
    private double sumaPriemerovPoctuVolnychPracovnikov2;
    
    /**
     * statistika 7 - priemerny pocet zakaznikov v rade prichodov
     */
    private double casPoslednejZmenyRaduPrichodov7;
    private double sumaPocetLudiVRadePrichodov7;
    private double sumaPriemerovPoctuLudiVRadePrichodov7;
    private double statistika7;
    
    /**
     * statistika 8
     */
    
    
    /**
     * pocet zakaznikov
     */
    private int pocetZakaznikovPoSkonceniZadaniaObjednavky2;
    private int pocetZakaznikovPoOdovzdaniOpravenehoAuta1;
    //sem 3 kontrola
    private int pocetZakaznikovPoSkonceniKontroly1111;
    
    /**
     * aktualna replikacia
     */
    private int aktualnaReplikacia;
    
    /**
     * animacne
     */
    private int pocet1;
    private int pocet2;
    private int pocetLudiVRade1;
    private int pocetAutVRade2;
    private int pocetAutVRade3;
    private boolean[] vybavovanieObjednavky;
    private boolean[] opravovanie;
    private boolean[] preparkovaniePlusOdovzdanie;
    
    public AutoservisStatistika() {
        this.sumaPriemerovCasovCakaniaNaOpravu1 = 0.0;
        this.sumaPriemerovCasovCakaniaNaOpravuNaDruhu1 = 0.0;
        this.sumaPriemerovCasovCakaniaNaOpravu1111 = 0.0;
        this.sumaPriemerovCasovCakaniaNaOpravuNaDruhu1111 = 0.0;
        
        this.sumaPriemerovCasovCakaniaVRadeNaZadanieObjednavky2 = 0.0;
        this.sumaPriemerovCasovCakaniaVRadeNaZadanieObjednavkyNaDruhu2 = 0.0;
        
        this.sumaPriemerovCasovStravenychZakaznikomVServise4 = 0.0;
        
        this.sumaPriemerovPoctuVolnychPracovnikov1 = 0.0;
        this.sumaPriemerovPoctuVolnychPracovnikov2 = 0.0;
        
        this.aktualnaReplikacia = 0;
        
        this.pocetLudiVRade1 =0;
        this.pocetAutVRade2 =0;
        this.pocetAutVRade3 =0;
    }
    
    public void vynuluj() {
        this.sumaCasovCakaniaNaOpravu1 = 0;
        this.sumaCasovCakaniaNaOpravu1111 = 0;
        this.sumaCasovCakaniaVRadeNaZadanieObjednavky2 = 0.0;
        this.sumaCasovStravenychZakaznikomVServise4 = 0.0;
        this.sumaPocetVolnychPracovnikov1 = 0;
        this.sumaPocetVolnychPracovnikov2 = 0;
        this.sumaPocetLudiVRadePrichodov7 = 0;
        
        this.pocetZakaznikovPoOdovzdaniOpravenehoAuta1 = 0;
        this.pocetZakaznikovPoSkonceniKontroly1111 = 0;
        this.pocetZakaznikovPoSkonceniZadaniaObjednavky2 = 0;
        
        this.casPoslednejZmenyObsluha1 = 30.0*8.0*60.0*60.0;
        this.casPoslednejZmenyObsluha2 = 30.0*8.0*60.0*60.0;//0.0;
        this.casPoslednejZmenyRaduPrichodov7 = 30.0*8.0*60.0*60.0;//0.0;
        
        vybavovanieObjednavky = new boolean[pocet1];
        Arrays.fill(vybavovanieObjednavky,false);
        opravovanie = new boolean[pocet2];
        Arrays.fill(opravovanie,false);
        preparkovaniePlusOdovzdanie = new boolean[pocet1];
        Arrays.fill(preparkovaniePlusOdovzdanie,false);
        pocetLudiVRade1 = 0;
        pocetAutVRade2 = 0;
        pocetAutVRade3 =0;
    }

    //statistika 1--------------------------------------------------------------------------------------------------------------------------------
    void pridajCasCakaniaNaOpravu1(double cas) {
        this.sumaCasovCakaniaNaOpravu1 += cas;
    }
    void pridajCasCakaniaNaOpravu1111(double cas) {
        this.sumaCasovCakaniaNaOpravu1111 += cas;
    }
    
    public void pridajPriemerCasuCakaniaNaOpravu1(double casPriemeru) {
        this.sumaPriemerovCasovCakaniaNaOpravu1 += casPriemeru;
        this.sumaPriemerovCasovCakaniaNaOpravuNaDruhu1 += casPriemeru*casPriemeru; //IS
    }
    public void pridajPriemerCasuCakaniaNaOpravu1111(double casPriemeru) {
        this.sumaPriemerovCasovCakaniaNaOpravu1111 += casPriemeru;
        this.sumaPriemerovCasovCakaniaNaOpravuNaDruhu1111 += casPriemeru*casPriemeru; //IS
    }

    /**
     * @return the sumaCasovCakaniaNaOpravu1
     */
    public double getSumaCasovCakaniaNaOpravu1() {
        return sumaCasovCakaniaNaOpravu1;
    }

    /**
     * @return the sumaCasovCakaniaNaOpravu1
     */
    public double getSumaCasovCakaniaNaOpravu1111() {
        return sumaCasovCakaniaNaOpravu1111;
    }

    /**
     * @return the sumaPriemerovCasovCakaniaNaOpravu1
     */
    public double getSumaPriemerovCasovCakaniaNaOpravu1() {
        return sumaPriemerovCasovCakaniaNaOpravu1;
    }

    /**
     * @return the sumaPriemerovCasovCakaniaNaOpravu1
     */
    public double getSumaPriemerovCasovCakaniaNaOpravu1111() {
        return sumaPriemerovCasovCakaniaNaOpravu1111;
    }

    /**
     * @return the sumaPriemerovCasovCakaniaNaOpravuNaDruhu1
     */
    public double getSumaPriemerovCasovCakaniaNaOpravuNaDruhu1() {
        return sumaPriemerovCasovCakaniaNaOpravuNaDruhu1;
    }

    /**
     * @return the sumaPriemerovCasovCakaniaNaOpravuNaDruhu1
     */
    public double getSumaPriemerovCasovCakaniaNaOpravuNaDruhu1111() {
        return sumaPriemerovCasovCakaniaNaOpravuNaDruhu1111;
    }
    
    
    // statistika 2-------------------------------------------------------------------------------------------------------------------------------
    void pridajCasCakaniaVRadeNaZadanieObjednavky2(double cas) {
        this.sumaCasovCakaniaVRadeNaZadanieObjednavky2 += cas;
    }
    
    public void pridajPriemerCasuCakaniaVRadeNaZadanieObjednavky2(double casPriemeru) {
        this.sumaPriemerovCasovCakaniaVRadeNaZadanieObjednavky2 += casPriemeru;
        this.sumaPriemerovCasovCakaniaVRadeNaZadanieObjednavkyNaDruhu2 += casPriemeru*casPriemeru;
    }
    
    /**
     * @return the sumaCasovCakaniaVRadeNaZadanieObjednavky2
     */
    public double getSumaCasovCakaniaVRadeNaZadanieObjednavky2() {
        return sumaCasovCakaniaVRadeNaZadanieObjednavky2;
    }

    /**
     * @return the sumaPriemerovCasovCakaniaVRadeNaZadanieObjednavky2
     */
    public double getSumaPriemerovCasovCakaniaVRadeNaZadanieObjednavky2() {
        return sumaPriemerovCasovCakaniaVRadeNaZadanieObjednavky2;
    }
    /**
     * @return the sumaPriemerovCasovCakaniaVRadeNaZadanieObjednavkyNaDruhu2
     */
    public double getSumaPriemerovCasovCakaniaVRadeNaZadanieObjednavkyNaDruhu2() {
        return sumaPriemerovCasovCakaniaVRadeNaZadanieObjednavkyNaDruhu2;
    }
    
    //statistika 4
    public void pridajCasStravenyZakaznikomVServise4(double cas) {
        this.sumaCasovStravenychZakaznikomVServise4+= cas;
    }
    
    public void pridajPriemerCasuStravenehoZakaznikomVServise4(double cas) {
        this.sumaPriemerovCasovStravenychZakaznikomVServise4 += cas;
    }

    /**
     * @return the sumaCasovStravenychZakaznikomVServise
     */
    public double getSumaCasovStravenychZakaznikomVServise4() {
        return sumaCasovStravenychZakaznikomVServise4;
    }

    /**
     * @return the sumaPriemerovCasovStravenychZakaznikomVServise
     */
    public double getSumaPriemerovCasovStravenychZakaznikomVServise4() {
        return sumaPriemerovCasovStravenychZakaznikomVServise4;
    }

    //statistika 5 priemerny pocet volnych pracovnikov typu 1-------------------------------------------------------------------------------------
    public void pridajPocetVolnychPracovnikov1(double casKratVaha) {
        this.sumaPocetVolnychPracovnikov1 += casKratVaha;
    }
    
    public void pridajPriemerPoctuVolnychPracovnikov1(double priemer) {
        this.sumaPriemerovPoctuVolnychPracovnikov1 += priemer;
    }

    /**
     * @return the sumaPocetVolnychPracovnikov1
     */
    public double getSumaPocetVolnychPracovnikov1() {
        return sumaPocetVolnychPracovnikov1;
    }

    /**
     * @return the sumaPriemerovPoctuVolnychPracovnikov1
     */
    public double getSumaPriemerovPoctuVolnychPracovnikov1() {
        return sumaPriemerovPoctuVolnychPracovnikov1;
    }
    
    /**
     * @return the casPoslednejZmenyObsluha1
     */
    public double getCasPoslednejZmenyObsluha1() {
        return casPoslednejZmenyObsluha1;
    }

    /**
     * @param cas the casPoslednejZmenyObsluha1 to set
     */
    public void setCasPoslednejZmenyObsluha1(double cas) {
        this.casPoslednejZmenyObsluha1 = cas;
    }
    //statistika 6 priemerny pocet volnych pracovnikov typu 2-------------------------------------------------------------------------------------
    public void pridajPocetVolnychPracovnikov2(double casKratVaha) {
        this.sumaPocetVolnychPracovnikov2 += casKratVaha;
    }
    
    public void pridajPriemerPoctuVolnychPracovnikov2(double priemer) {
        this.sumaPriemerovPoctuVolnychPracovnikov2 += priemer;
    }

    /**
     * @return the sumaPocetVolnychPracovnikov2
     */
    public double getSumaPocetVolnychPracovnikov2() {
        return sumaPocetVolnychPracovnikov2;
    }

    /**
     * @return the sumaPriemerovPoctuVolnychPracovnikov2
     */
    public double getSumaPriemerovPoctuVolnychPracovnikov2() {
        return sumaPriemerovPoctuVolnychPracovnikov2;
    }
    
    /**
     * @return the casPoslednejZmenyObsluha2
     */
    public double getCasPoslednejZmenyObsluha2() {
        return casPoslednejZmenyObsluha2;
    }

    /**
     * @param cas the casPoslednejZmenyObsluha1 to set
     */
    public void setCasPoslednejZmenyObsluha2(double cas) {
        this.casPoslednejZmenyObsluha2 = cas;
    }
    //statistika 7 priemerny pocet ludi v rade prichodov------------------------------------------------------------------------------------------
    public void pridajPocetLudiVRadePrichodov7(double casKratVaha) {
        this.sumaPocetLudiVRadePrichodov7 += casKratVaha;
    }
    
    public void pridajPriemerPoctuLudiVRadePrichodov7(double priemer) {
        this.sumaPriemerovPoctuLudiVRadePrichodov7+= priemer;
    }

    /**
     * @return the casPoslednejZmenyRaduPrichodov7
     */
    public double getCasPoslednejZmenyRaduPrichodov7() {
        return casPoslednejZmenyRaduPrichodov7;
    }

    /**
     * @param casPoslednejZmenyRaduPrichodov7 the casPoslednejZmenyRaduPrichodov7 to set
     */
    public void setCasPoslednejZmenyRaduPrichodov7(double casPoslednejZmenyRaduPrichodov7) {
        this.casPoslednejZmenyRaduPrichodov7 = casPoslednejZmenyRaduPrichodov7;
    }

    /**
     * @return the sumaPocetLudiVRadePrichodov7
     */
    public double getSumaPocetLudiVRadePrichodov7() {
        return sumaPocetLudiVRadePrichodov7;
    }

    /**
     * @return the sumaPriemerovPoctuLudiVRadePrichodov7
     */
    public double getSumaPriemerovPoctuLudiVRadePrichodov7() {
        return sumaPriemerovPoctuLudiVRadePrichodov7;
    }
    
    //pocet zakaznikov----------------------------------------------------------------------------------------------------------------------------
    public void pridajZakaznikaPoSkonceniZadaniaObjednavky2() {
        this.pocetZakaznikovPoSkonceniZadaniaObjednavky2++;
    }
    public void pridajZakaznikaPoOdovzdaniOpravenehoAuta1() {
        this.pocetZakaznikovPoOdovzdaniOpravenehoAuta1++;
    }

    /**
     * @return the pocetZakaznikovPoOdovzdaniOpravenehoAuta1
     */
    public int getPocetZakaznikovPoOdovzdaniOpravenehoAuta1() {
        return pocetZakaznikovPoOdovzdaniOpravenehoAuta1;
    }

    /**
     * @return the pocetZakaznikovPoSkonceniZadaniaObjednavky2
     */
    public int getPocetZakaznikovPoSkonceniZadaniaObjednavky2() {
        return pocetZakaznikovPoSkonceniZadaniaObjednavky2;
    }
    
    //aktualna replikacia-------------------------------------------------------
    public void aktualizujAktualnuReplikaciu() {
        this.aktualnaReplikacia++;
    }

    /**
     * @return the aktualnaReplikacia
     */
    public int getAktualnaReplikacia() {
        return aktualnaReplikacia;
    }
    
    //interval spolahlivosti
    public double[] vypocitajIntervalSpolahlivosti(double XiNaDruhu, double Xi) {
        //double t_alpha = 1.645;//95%
        double t_alpha = 1.28;//95%
        double s = Math.sqrt(XiNaDruhu/getAktualnaReplikacia() - Math.pow(Xi/getAktualnaReplikacia(),2.0));
        double dolnaHranica = Xi/getAktualnaReplikacia() - ((t_alpha*s)/Math.sqrt((getAktualnaReplikacia()-1)));
        double hornaHranica = Xi/getAktualnaReplikacia() + ((t_alpha*s)/Math.sqrt((getAktualnaReplikacia()-1)));
        //System.out.println("<"+dolnaHranica+", "+hornaHranica+">");
        return new double[]{dolnaHranica,hornaHranica};
    }

    void inicializacia(int p1,int p2) {
        this.pocet1 = p1;
        this.pocet2 = p2;
    }
    
    void nastavVybavovanie(int index) {
        vybavovanieObjednavky[index] = true;
    }
   
    void nastavOpravovanie(int index) {
        opravovanie[index] = true;
    }
    
    void nastavPreparkovaniaOdovzdanie(int index) {
        preparkovaniePlusOdovzdanie[index] = true;
    }
    
    void zrusVybavovanie(int index) {
        vybavovanieObjednavky[index] = false;
    }
   
    void zrusOpravovanie(int index) {
        opravovanie[index] = false;
    }
    
    void zrusPreparkovaniaOdovzdanie(int index) {
        preparkovaniePlusOdovzdanie[index] = false;
    }
    
    void pridajPocetLudiVRade1() {
        pocetLudiVRade1++;
    }
    
    void pridajPocetAutVRade2() {
        pocetAutVRade2++;
    }
    
    void pridajPocetAutVRade3() {
        pocetAutVRade3++;
    }
    
    void odoberPocetLudiVRade1() {
        pocetLudiVRade1--;
    }
    
    void odoberPocetAutVRade2() {
        pocetAutVRade2--;
    }
    
    void odoberPocetAutVRade3() {
        pocetAutVRade3--;
    }

    /**
     * @return the pocetLudiVRade1
     */
    public int getPocetLudiVRade1() {
        return pocetLudiVRade1;
    }
    
    public void vynulujPocetLudiVRade1() {
        pocetLudiVRade1 = 0;
    }

    /**
     * @return the pocetAutVRade2
     */
    public int getPocetAutVRade2() {
        return pocetAutVRade2;
    }

    /**
     * @return the pocetAutVRade3
     */
    public int getPocetAutVRade3() {
        return pocetAutVRade3;
    }
    
    public String getPracovniciNaObjednavani() {
        String res = "";
        for(int i=0;i<vybavovanieObjednavky.length;i++) {
            if(vybavovanieObjednavky[i])
                res += (i+1)+", ";
        }
        return res;
    }
    
    public String getPracovniciNaPreparkovaniPlusOdovzdani() {
        String res = "";
        for(int i=0;i<preparkovaniePlusOdovzdanie.length;i++) {
            if(preparkovaniePlusOdovzdanie[i])
                res += (i+1)+", ";
        }
        return res;
    }
    
    public String getPracovniciNaOprave() {
        String res = "";
        for(int i=0;i<opravovanie.length;i++) {
            if(opravovanie[i])
                res += (i+1)+", ";
        }
        return res;
    }

    /**
     * @return the statistika1
     */
    public double getStatistika1() {
        return statistika1;
    }

    /**
     * @param statistika1 the statistika1 to set
     */
    public void setStatistika1(double statistika1) {
        this.statistika1 = statistika1;
    }

    /**
     * @return the statistika1111
     */
    public double getStatistika1111() {
        return statistika1111;
    }

    /**
     * @param statistika1111 the statistika1111 to set
     */
    public void setStatistika1111(double statistika1111) {
        this.statistika1111 = statistika1111;
    }

    /**
     * @return the statistika2
     */
    public double getStatistika2() {
        return statistika2;
    }

    /**
     * @param statistika2 the statistika2 to set
     */
    public void setStatistika2(double statistika2) {
        this.statistika2 = statistika2;
    }

    /**
     * @return the statistika7
     */
    public double getStatistika7() {
        return statistika7;
    }

    /**
     * @param statistika7 the statistika7 to set
     */
    public void setStatistika7(double statistika7) {
        this.statistika7 = statistika7;
    }

    /**
     * @return the statistika4
     */
    public double getStatistika4() {
        return statistika4;
    }

    /**
     * @param statistika4 the statistika4 to set
     */
    public void setStatistika4(double statistika4) {
        this.statistika4 = statistika4;
    }

    /**
     * @return the is1
     */
    public double[] getIs1() {
        return is1;
    }

    /**
     * @param is1 the is1 to set
     */
    public void setIs1(double[] is1) {
        this.is1 = is1;
    }

    /**
     * @return the is2
     */
    public double[] getIs2() {
        return is2;
    }

    /**
     * @param is2 the is2 to set
     */
    public void setIs2(double[] is2) {
        this.is2 = is2;
    }

    /**
     * @return the pocetZakaznikovPoSkonceniKontroly1111
     */
    public int getPocetZakaznikovPoSkonceniKontroly1111() {
        return pocetZakaznikovPoSkonceniKontroly1111;
    }
    public void pridajZakaznikaPoSkonceniKontroly1111() {
        this.pocetZakaznikovPoSkonceniKontroly1111++;
    }
}
