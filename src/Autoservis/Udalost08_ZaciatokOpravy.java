/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Autoservis;

import udalostnaSimulacia.SimulacneJadro;

/**
 *
 * @author Robo
 */
public class Udalost08_ZaciatokOpravy extends AutoservisUdalost {

    private AutoservisSimulacneJadro jadroAutoservis;

    public Udalost08_ZaciatokOpravy(double casVyskytu, SimulacneJadro simJadro, AutoservisZakaznik zakaznik) {
        super(casVyskytu, simJadro, zakaznik);
        this.jadroAutoservis = (AutoservisSimulacneJadro) simJadro;
    }

    @Override
    public void execute() {
        /**
         *  atributy------------------------------------------------------------
         */
        
        /**
         *  povinne udalosti----------------------------------------------------
         */
         //naplanovanie konca opravy - udalost 9
        double casKoncaOpravy = this.getCasVyskytu() + this.getZakaznik().getCelkovyCasOpravy();
        jadroAutoservis.pridajUdalost(new Udalost09_KoniecOpravy(casKoncaOpravy, jadroAutoservis, this.getZakaznik()));
        
        /**
         *  volitelne udalosti--------------------------------------------------
         */
        
        /**
         *  statistika----------------------------------------------------------
         */
    }
    
}
