/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Autoservis;

import java.util.logging.Level;
import java.util.logging.Logger;
import udalostnaSimulacia.SimulacneJadro;

/**
 *
 * @author Robo
 */
public class Udalost_Animacia extends AutoservisUdalost {

    private AutoservisSimulacneJadro jadroAutoservis;

    public Udalost_Animacia(double casVyskytu, SimulacneJadro simJadro, AutoservisZakaznik zakaznik) {
        super(casVyskytu, simJadro, zakaznik);
        jadroAutoservis = (AutoservisSimulacneJadro) simJadro;
    }

    @Override
    public void execute() {
        /**
         *  atributy------------------------------------------------------------
         */
        
        /**
         *  povinne udalosti----------------------------------------------------
         */
        if(jadroAutoservis.isAnimacia() ) {
            double casDalsejAnimacie = this.getCasVyskytu() + jadroAutoservis.getPeriodaAnimacie();
            jadroAutoservis.pridajUdalost(new Udalost_Animacia(casDalsejAnimacie, jadroAutoservis, null));
            jadroAutoservis.obnovGui(jadroAutoservis);
            //toDo
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Udalost_Animacia.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        /**
         *  volitelne udalosti--------------------------------------------------
         */
        
        /**
         *  statistika----------------------------------------------------------
         */
    }
    
}
