/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Autoservis;

import udalostnaSimulacia.SimulacneJadro;

/**
 *
 * @author Robo
 */
public class Udalost11_KoniecPreparkovaniaAutaZDielne extends AutoservisUdalost {

    private AutoservisSimulacneJadro jadroAutoservis;

    public Udalost11_KoniecPreparkovaniaAutaZDielne(double casVyskytu, SimulacneJadro simJadro, AutoservisZakaznik zakaznik) {
        super(casVyskytu, simJadro, zakaznik);
        this.jadroAutoservis = (AutoservisSimulacneJadro) simJadro;
    }

    @Override
    public void execute() {
        /**
         *  atributy------------------------------------------------------------
         */
        
        /**
         *  povinne udalosti----------------------------------------------------
         */
        jadroAutoservis.pridajUdalost(new Udalost12_ZaciatokPrevzatiaOpravenehoAuta(this.getCasVyskytu(), jadroAutoservis, this.getZakaznik()));
        
        /**
         *  volitelne udalosti--------------------------------------------------
         */
        
        /**
         *  statistika----------------------------------------------------------
         */
    }
    
}
