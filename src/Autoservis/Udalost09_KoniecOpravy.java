/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Autoservis;

import udalostnaSimulacia.SimulacneJadro;

/**
 *
 * @author Robo
 */
public class Udalost09_KoniecOpravy extends AutoservisUdalost {

    private AutoservisSimulacneJadro jadroAutoservis;

    public Udalost09_KoniecOpravy(double casVyskytu, SimulacneJadro simJadro, AutoservisZakaznik zakaznik) {
        super(casVyskytu, simJadro, zakaznik);
        this.jadroAutoservis = (AutoservisSimulacneJadro) simJadro;
    }

    @Override
    public void execute() {
           
        /**
         *  atributy------------------------------------------------------------
         */
        jadroAutoservis.uvolniPracovnikaTypu2(this.getZakaznik().getIdxPracovnikaTypu2());
        jadroAutoservis.getStatistika().zrusOpravovanie(this.getZakaznik().getIdxPracovnikaTypu2());//animacna statistika 4
        
        
        /**
         *  povinne udalosti----------------------------------------------------
         */
        
        /**
         *  volitelne udalosti--------------------------------------------------
         */
        //naplanovanie novej opravy, ak nie je rad cakajucich aut na opravu prazdny - udalost 8
        boolean menimObsluhu2 = true;
        if(!jadroAutoservis.jeFrontAutCakajucichNaOpravuPrazdny()) {
            menimObsluhu2 = false;
            AutoservisZakaznik z = jadroAutoservis.odoberZFrontuAutCakajucichNaOpravu();
            jadroAutoservis.getStatistika().odoberPocetAutVRade2();//animacna statistika 3
            z.setIdxPracovnikaTypu2(this.getZakaznik().getIdxPracovnikaTypu2());
            jadroAutoservis.obsadPracovnikaTypu2(this.getZakaznik().getIdxPracovnikaTypu2());
            jadroAutoservis.getStatistika().nastavOpravovanie(this.getZakaznik().getIdxPracovnikaTypu2());//animacna statistika 4
            jadroAutoservis.pridajUdalost(new Udalost08_ZaciatokOpravy(this.getCasVyskytu(), jadroAutoservis, z));
        }
        
        //naplanovanie preparkovania auta na parkovisko, ak je volny pracovnik obsluhy1, inak do radu - udalost 10
        int idxVolnehoPracovnikaTypu1 = jadroAutoservis.getVolnyPracovnik1();
        boolean menimObsluhu1 = false;
        if(idxVolnehoPracovnikaTypu1 >= 0) {
            menimObsluhu1 = true;
            this.getZakaznik().setIdxPracovnikaTypu1Preparkovanie(idxVolnehoPracovnikaTypu1);
            jadroAutoservis.obsadPracovnikaTypu1(idxVolnehoPracovnikaTypu1);
            jadroAutoservis.getStatistika().nastavPreparkovaniaOdovzdanie(idxVolnehoPracovnikaTypu1);//animacna statistika 6
            jadroAutoservis.pridajUdalost(new Udalost10_ZaciatokPreparkovaniaAutaZDielne(this.getCasVyskytu(), jadroAutoservis, this.getZakaznik()));
        } else {
            jadroAutoservis.pridajDoFrontuAutCakajuchNaPreparkovanieZDielne(this.getZakaznik());
            jadroAutoservis.getStatistika().pridajPocetAutVRade3();//animacna statistika 5
        }
        
        /**
         *  statistika----------------------------------------------------------
         */
        if(jadroAutoservis.isZahriate()) {
            //SEM 3 KONTROLA
            jadroAutoservis.getStatistika().pridajCasCakaniaNaOpravu1111(this.getCasVyskytu() - this.getZakaznik().getCasZaciatkuCakaniaNaOpravuPoOdovzdaniAuta1());
            jadroAutoservis.getStatistika().pridajZakaznikaPoSkonceniKontroly1111();
            //SEM 3 kontrola
            
            
            
            //statistika 5 - priemerny pocet volnych pracovnikov typu 1
            if(menimObsluhu1) {
                jadroAutoservis.getStatistika().pridajPocetVolnychPracovnikov1(
                        (this.getCasVyskytu() - jadroAutoservis.getStatistika().getCasPoslednejZmenyObsluha1())*(jadroAutoservis.getPocetVolnychPracovnikov1()+1));
                jadroAutoservis.getStatistika().setCasPoslednejZmenyObsluha1(this.getCasVyskytu());
            }
            //statistika 6 - priemerny pocet volnych pracovnikov typu 2
            if(menimObsluhu2) {
                jadroAutoservis.getStatistika().pridajPocetVolnychPracovnikov2(
                        (this.getCasVyskytu() - jadroAutoservis.getStatistika().getCasPoslednejZmenyObsluha2())*(jadroAutoservis.getPocetVolnychPracovnikov2()-1));
                jadroAutoservis.getStatistika().setCasPoslednejZmenyObsluha2(this.getCasVyskytu());
            }
        }
    }
    
}
