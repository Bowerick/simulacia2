/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Autoservis;

import udalostnaSimulacia.SimulacneJadro;

/**
 *
 * @author Robo
 */
public class Udalost07_KoniecPreparkovaniaAutaDoDielne extends AutoservisUdalost {

    private AutoservisSimulacneJadro jadroAutoservis;

    public Udalost07_KoniecPreparkovaniaAutaDoDielne(double casVyskytu, SimulacneJadro simJadro, AutoservisZakaznik zakaznik) {
        super(casVyskytu, simJadro, zakaznik);
        this.jadroAutoservis = (AutoservisSimulacneJadro) simJadro;
    }

    @Override
    public void execute() {
        
        /**
         *  atributy------------------------------------------------------------
         */
        
        /**
         *  povinne udalosti----------------------------------------------------
         */
        jadroAutoservis.uvolniPracovnikaTypu1(this.getZakaznik().getIdxPracovnikaTypu1Objednavka());
        jadroAutoservis.getStatistika().zrusVybavovanie(this.getZakaznik().getIdxPracovnikaTypu1Objednavka());//animacna statistika 2
        boolean menimObsluhu1 = true;
        
        //ak je rad aut cakajucich na opravu prazdny, a zaroven je volna obsluha2, tak naplanujem opravu, inak ide auto do radu - udalost 8
        int idxVolnehoPracovnikaTypu2 = jadroAutoservis.getVolnyPracovnik2(); 
        boolean menimObsluhu2 = false;
        if(jadroAutoservis.jeFrontAutCakajucichNaOpravuPrazdny() && idxVolnehoPracovnikaTypu2 >= 0) {
            menimObsluhu2 = true;
            this.getZakaznik().setIdxPracovnikaTypu2(idxVolnehoPracovnikaTypu2);
            jadroAutoservis.obsadPracovnikaTypu2(idxVolnehoPracovnikaTypu2);
            jadroAutoservis.getStatistika().nastavOpravovanie(idxVolnehoPracovnikaTypu2);//animacna statistika 4
            jadroAutoservis.pridajUdalost(new Udalost08_ZaciatokOpravy(this.getCasVyskytu(), jadroAutoservis, this.getZakaznik()));
        } else {
            jadroAutoservis.pridajDoFrontuAutCakajucichNaOpravu(this.getZakaznik());
            jadroAutoservis.getStatistika().pridajPocetAutVRade2();//animacna statistika 3
        }
        
        //ak v dielni caka auto na preparkovanie preparkujem ho, - udalost 10
        //inak ak nie je rad zakaznikov prazdny, obluzim dalsieho zakaznika v rade na objednavky - udalost 2
        int idxVolnehoPracovnikaTypu1 = this.getZakaznik().getIdxPracovnikaTypu1Objednavka();  
        boolean menimRadPrichodov = false;
        if(!jadroAutoservis.jeFrontAutCakajucichNaPreparkovanieZDielnePrazdny() ) {
            menimObsluhu1 = false;
            menimRadPrichodov = true;
            AutoservisZakaznik z = jadroAutoservis.odoberZFrontuAutCakajuchNaPreparkovanieZDielne();
            jadroAutoservis.obsadPracovnikaTypu1(idxVolnehoPracovnikaTypu1);
            jadroAutoservis.getStatistika().nastavPreparkovaniaOdovzdanie(idxVolnehoPracovnikaTypu1);//animacna statistika 6
            jadroAutoservis.getStatistika().odoberPocetAutVRade3();//animacna statistika 5
            z.setIdxPracovnikaTypu1Preparkovanie(idxVolnehoPracovnikaTypu1);//
            jadroAutoservis.pridajUdalost(new Udalost10_ZaciatokPreparkovaniaAutaZDielne(this.getCasVyskytu(), jadroAutoservis, z));
        } else {
            if(!jadroAutoservis.jeFrontZakaznikovPriPrichodePrazdny()) {
                menimObsluhu1 = false;
                AutoservisZakaznik z = jadroAutoservis.odoberZFrontuZakaznikovPriPrichode();
                jadroAutoservis.getStatistika().odoberPocetLudiVRade1();//animacna statistika
                z.setIdxPracovnikaTypu1Objednavka(idxVolnehoPracovnikaTypu1);
                jadroAutoservis.getStatistika().nastavVybavovanie(idxVolnehoPracovnikaTypu1);//animacna statistika 2
                jadroAutoservis.obsadPracovnikaTypu1(idxVolnehoPracovnikaTypu1);
                jadroAutoservis.pridajUdalost(new Udalost02_ZaciatokPrevzatiaObjednavky(this.getCasVyskytu(), jadroAutoservis, z));
            }
        }
        /**
         *  volitelne udalosti--------------------------------------------------
         */
        
        /**
         *  statistika----------------------------------------------------------
         */
        if(jadroAutoservis.isZahriate()) {
            //statistika 5 - priemerny pocet volnych pracovnikov typu 1
            if(menimObsluhu1) {
                jadroAutoservis.getStatistika().pridajPocetVolnychPracovnikov1(
                        (this.getCasVyskytu() - jadroAutoservis.getStatistika().getCasPoslednejZmenyObsluha1())*(jadroAutoservis.getPocetVolnychPracovnikov1()-1));
                jadroAutoservis.getStatistika().setCasPoslednejZmenyObsluha1(this.getCasVyskytu());
            }
            //statistika 6 - priemerny pocet volnych pracovnikov typu 2
            if(menimObsluhu2) {
                jadroAutoservis.getStatistika().pridajPocetVolnychPracovnikov2(
                        (this.getCasVyskytu() - jadroAutoservis.getStatistika().getCasPoslednejZmenyObsluha2())*(jadroAutoservis.getPocetVolnychPracovnikov2()+1));
                jadroAutoservis.getStatistika().setCasPoslednejZmenyObsluha2(this.getCasVyskytu());
            }
            //statistika 7 - priemerna dlzka radu zakaznikov pri prichode
            if(menimRadPrichodov) {
                jadroAutoservis.getStatistika().pridajPocetLudiVRadePrichodov7(
                        (this.getCasVyskytu() - jadroAutoservis.getStatistika().getCasPoslednejZmenyRaduPrichodov7())*(jadroAutoservis.getDlzkaFrontuPrichodovZakaznikov()+1));
                jadroAutoservis.getStatistika().setCasPoslednejZmenyRaduPrichodov7(this.getCasVyskytu());
            }
        }
    }
    
}
