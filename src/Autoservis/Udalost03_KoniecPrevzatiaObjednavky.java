/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Autoservis;

import udalostnaSimulacia.SimulacneJadro;

/**
 *
 * @author Robo
 */
public class Udalost03_KoniecPrevzatiaObjednavky extends AutoservisUdalost {

    private AutoservisSimulacneJadro jadroAutoservis;

    public Udalost03_KoniecPrevzatiaObjednavky(double casVyskytu, SimulacneJadro simJadro, AutoservisZakaznik zakaznik) {
        super(casVyskytu, simJadro, zakaznik);
        this.jadroAutoservis = (AutoservisSimulacneJadro) simJadro;
    }

    @Override
    public void execute() {
        /**
         *  atributy------------------------------------------------------------
         */
        
        /**
         *  povinne udalosti----------------------------------------------------
         */
        jadroAutoservis.pridajUdalost(new Udalost04_ZaciatokPrevzatiaAuta(this.getCasVyskytu(), jadroAutoservis, this.getZakaznik()));
        /**
         *  volitelne udalosti--------------------------------------------------
         */
        
        /**
         *  statistika----------------------------------------------------------
         */
    }
    
}
