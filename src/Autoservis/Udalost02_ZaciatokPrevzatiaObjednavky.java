/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Autoservis;

import udalostnaSimulacia.SimulacneJadro;

/**
 *
 * @author Robo
 */
public class Udalost02_ZaciatokPrevzatiaObjednavky extends AutoservisUdalost {

    private AutoservisSimulacneJadro jadroAutoservis;

    public Udalost02_ZaciatokPrevzatiaObjednavky(double casVyskytu, SimulacneJadro simJadro, AutoservisZakaznik zakaznik) {
        super(casVyskytu, simJadro, zakaznik);
        this.jadroAutoservis = (AutoservisSimulacneJadro) simJadro;
    }

    @Override
    public void execute() {
        /**
         *  atributy------------------------------------------------------------
         */
        
        //generovanie oprav
        int celkovyCasOpravy = 0;
        int pocetOprav = jadroAutoservis.dalsiPocetOprav();
        this.getZakaznik().inicializujPolia(pocetOprav);
        for(int i=0;i<pocetOprav;i++) {
            int typOpravy = jadroAutoservis.dalsiTypOpravy();
            this.getZakaznik().setTypOpravy(i, typOpravy);
            int dlzkaOpravy = -1;
            if(typOpravy == 1) {
                dlzkaOpravy = jadroAutoservis.dalsiCasDlzkyJednoduchejOpravy();
            }
            if(typOpravy == 2) {
                int typStredneTazkejOpravy = jadroAutoservis.dalsiTypStredneTazkejOpravy();
                this.getZakaznik().setTypOpravy2(i, typStredneTazkejOpravy);
                if(typStredneTazkejOpravy == 1) {
                    dlzkaOpravy = jadroAutoservis.dalsiCasDlzkyStredneTazkejOpravy1();
                }
                if(typStredneTazkejOpravy == 2) {
                    dlzkaOpravy = jadroAutoservis.dalsiCasDlzkyStredneTazkejOpravy2();
                }
                if(typStredneTazkejOpravy == 3) {
                    dlzkaOpravy = jadroAutoservis.dalsiCasDlzkyStredneTazkejOpravy3();
                }
            }
            if(typOpravy == 3) {
                dlzkaOpravy = jadroAutoservis.dalsiCasDlzkyZlozitejOpravy();
            }
            this.getZakaznik().setDlzkaOpravy(i,dlzkaOpravy);
            celkovyCasOpravy += dlzkaOpravy;
        }
        this.getZakaznik().setCelkovyCasOpravy(celkovyCasOpravy);
        
        
        /**
         *  povinne udalosti----------------------------------------------------
         */
        double casKoncaPrevzatiaObjednavky = this.getCasVyskytu() + jadroAutoservis.dalsiCasDlzkyPrevzatieObjednavky();
        jadroAutoservis.pridajUdalost(new Udalost03_KoniecPrevzatiaObjednavky(casKoncaPrevzatiaObjednavky, jadroAutoservis, this.getZakaznik()));
        
        /**
         *  volitelne udalosti--------------------------------------------------
         */
        
        /**
         *  statistika----------------------------------------------------------
         */
        if(jadroAutoservis.isZahriate()) {
            jadroAutoservis.getStatistika().pridajCasCakaniaVRadeNaZadanieObjednavky2(this.getCasVyskytu()-this.getZakaznik().getCasPrichodu());
            jadroAutoservis.getStatistika().pridajZakaznikaPoSkonceniZadaniaObjednavky2();
        }
    }
    
}
