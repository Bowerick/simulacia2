/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Autoservis;

import udalostnaSimulacia.SimulacneJadro;

/**
 *
 * @author Robo
 */
public class Udalost13_KoniecPrevzatiaOpravenehoAuta extends AutoservisUdalost {

    private AutoservisSimulacneJadro jadroAutoservis;

    public Udalost13_KoniecPrevzatiaOpravenehoAuta(double casVyskytu, SimulacneJadro simJadro, AutoservisZakaznik zakaznik) {
        super(casVyskytu, simJadro, zakaznik);
        this.jadroAutoservis = (AutoservisSimulacneJadro) simJadro;
    }

    @Override
    public void execute() {
        
        /**
         *  atributy------------------------------------------------------------
         */
        jadroAutoservis.uvolniPracovnikaTypu1(this.getZakaznik().getIdxPracovnikaTypu1Preparkovanie());
        jadroAutoservis.getStatistika().zrusPreparkovaniaOdovzdanie(this.getZakaznik().getIdxPracovnikaTypu1Preparkovanie());//animacna statistika 6
        boolean menimObsluhu1 = true;//statistika - priemerny pocet volnych pracovnikov 1
        
        /**
         *  povinne udalosti----------------------------------------------------
         */
        
        /**
         *  volitelne udalosti--------------------------------------------------
         */
        //ak nie je front aut cakajucich na preparkovanie prazdny, tak preparkuj a odovzdaj dalsie auto, - udalost 10
        //inak ak nie je front zakaznikov prazdny vybav dalsiu objednavku - udalost 2
        boolean menimRadPrichodov = false;
        if(!jadroAutoservis.jeFrontAutCakajucichNaPreparkovanieZDielnePrazdny()) {
            menimObsluhu1 = false;
            AutoservisZakaznik z = jadroAutoservis.odoberZFrontuAutCakajuchNaPreparkovanieZDielne();
            jadroAutoservis.getStatistika().odoberPocetAutVRade3();//animacna statistika 5
            z.setIdxPracovnikaTypu1Preparkovanie(this.getZakaznik().getIdxPracovnikaTypu1Preparkovanie());
            jadroAutoservis.obsadPracovnikaTypu1(this.getZakaznik().getIdxPracovnikaTypu1Preparkovanie());
            jadroAutoservis.getStatistika().nastavPreparkovaniaOdovzdanie(this.getZakaznik().getIdxPracovnikaTypu1Preparkovanie());//animacna statistika 6
            jadroAutoservis.pridajUdalost(new Udalost10_ZaciatokPreparkovaniaAutaZDielne(this.getCasVyskytu(), jadroAutoservis, z));
        } else {
            if(!jadroAutoservis.jeFrontZakaznikovPriPrichodePrazdny()) {
                menimObsluhu1 = false;
                menimRadPrichodov = true;
                AutoservisZakaznik z = jadroAutoservis.odoberZFrontuZakaznikovPriPrichode();
                jadroAutoservis.getStatistika().odoberPocetLudiVRade1();//animacna statistika
                jadroAutoservis.obsadPracovnikaTypu1(this.getZakaznik().getIdxPracovnikaTypu1Preparkovanie());
                jadroAutoservis.getStatistika().zrusVybavovanie(this.getZakaznik().getIdxPracovnikaTypu1Objednavka());//animacna statistika 2
                z.setIdxPracovnikaTypu1Objednavka(this.getZakaznik().getIdxPracovnikaTypu1Preparkovanie());////
                jadroAutoservis.getStatistika().nastavVybavovanie(this.getZakaznik().getIdxPracovnikaTypu1Preparkovanie());//animacna statistika 2
                jadroAutoservis.pridajUdalost(new Udalost02_ZaciatokPrevzatiaObjednavky(this.getCasVyskytu(), jadroAutoservis, z));
            }
        }
        
        /**
         *  statistika----------------------------------------------------------
         */
        if(jadroAutoservis.isZahriate()) {
            //statistika 1
            jadroAutoservis.getStatistika().pridajCasCakaniaNaOpravu1(this.getCasVyskytu() - this.getZakaznik().getCasZaciatkuCakaniaNaOpravuPoOdovzdaniAuta1());
            jadroAutoservis.getStatistika().pridajZakaznikaPoOdovzdaniOpravenehoAuta1();
            //statistika 4
            jadroAutoservis.getStatistika().pridajCasStravenyZakaznikomVServise4(this.getCasVyskytu() - this.getZakaznik().getCasPrichodu());
            //statistika 5 - priemerny pocet volnych pracovnikov typu 1
            if(menimObsluhu1) {
                jadroAutoservis.getStatistika().pridajPocetVolnychPracovnikov1(
                        (this.getCasVyskytu() - jadroAutoservis.getStatistika().getCasPoslednejZmenyObsluha1())*(jadroAutoservis.getPocetVolnychPracovnikov1()-1));
                jadroAutoservis.getStatistika().setCasPoslednejZmenyObsluha1(this.getCasVyskytu());
            }
            //statistika 7 - priemerny pocet ludi v rade prichodov
            if(menimRadPrichodov) {
                jadroAutoservis.getStatistika().pridajPocetLudiVRadePrichodov7(
                        (this.getCasVyskytu() - jadroAutoservis.getStatistika().getCasPoslednejZmenyRaduPrichodov7())*(jadroAutoservis.getDlzkaFrontuPrichodovZakaznikov()+1));
                jadroAutoservis.getStatistika().setCasPoslednejZmenyRaduPrichodov7(this.getCasVyskytu());
            }
        }
    }
    
}
