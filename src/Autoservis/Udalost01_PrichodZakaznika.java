/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Autoservis;

import udalostnaSimulacia.SimulacneJadro;

/**
 *
 * @author Robo
 */
public class Udalost01_PrichodZakaznika extends AutoservisUdalost {

    private AutoservisSimulacneJadro jadroAutoservis;
    
    public Udalost01_PrichodZakaznika(double casVyskytu, SimulacneJadro simJadro, AutoservisZakaznik zakaznik) {
        super(casVyskytu, simJadro, zakaznik);
        this.jadroAutoservis = (AutoservisSimulacneJadro) simJadro;
    }

    @Override
    public void execute() {
        /**
         *  atributy
         */
        this.getZakaznik().setCasPrichodu(this.getCasVyskytu());
        
        
        /**
         *  povinne udalosti----------------------------------------------------
         */
        //prichod dalsieho zakaznika - udalost 1
        double prichodDalsiehoZakaznika = this.getCasVyskytu() + jadroAutoservis.dalsiCasPrichoduZakaznika();
        jadroAutoservis.pridajUdalost(new Udalost01_PrichodZakaznika(prichodDalsiehoZakaznika, jadroAutoservis, new AutoservisZakaznik()));
        
        
        /**
         *  volitelne udalosti--------------------------------------------------
         */
        //ak je rad zakaznikov prazdny, a zaroven je volna obluha1, tak zakaznik zada objednavku, inak ide do radu - udalost 2
        int idxVolnehoPracovnikaTypu1 = jadroAutoservis.getVolnyPracovnik1();     
        boolean menimObsluhu1 = false;
        boolean menimRadPrichodov = false;
        if(jadroAutoservis.jeFrontZakaznikovPriPrichodePrazdny() && idxVolnehoPracovnikaTypu1 >= 0) {
            menimObsluhu1 = true;
            this.getZakaznik().setIdxPracovnikaTypu1Objednavka(idxVolnehoPracovnikaTypu1);
            jadroAutoservis.obsadPracovnikaTypu1(idxVolnehoPracovnikaTypu1);
            jadroAutoservis.getStatistika().nastavVybavovanie(idxVolnehoPracovnikaTypu1);//animacna statistika 2
            jadroAutoservis.pridajUdalost(new Udalost02_ZaciatokPrevzatiaObjednavky(this.getCasVyskytu(), jadroAutoservis, this.getZakaznik()));
        } else {
            menimRadPrichodov = true;
            jadroAutoservis.pridajDoFrontuZakaznikovPriPrichode(this.getZakaznik());
            jadroAutoservis.getStatistika().pridajPocetLudiVRade1();//animacna statistika
        }
        
        /**
         *  statistika----------------------------------------------------------
         */
        if(jadroAutoservis.isZahriate()) {
            //statistika 5 - priemerny pocet volnych pracovnikov typu 1
            if(menimObsluhu1) {
                jadroAutoservis.getStatistika().pridajPocetVolnychPracovnikov1(
                        (this.getCasVyskytu() - jadroAutoservis.getStatistika().getCasPoslednejZmenyObsluha1())*(jadroAutoservis.getPocetVolnychPracovnikov1()+1));
                jadroAutoservis.getStatistika().setCasPoslednejZmenyObsluha1(this.getCasVyskytu());
            }
            //statistika 7 -
            if(menimRadPrichodov) {
                jadroAutoservis.getStatistika().pridajPocetLudiVRadePrichodov7(
                        (this.getCasVyskytu() - jadroAutoservis.getStatistika().getCasPoslednejZmenyRaduPrichodov7())*(jadroAutoservis.getDlzkaFrontuPrichodovZakaznikov()-1));
                jadroAutoservis.getStatistika().setCasPoslednejZmenyRaduPrichodov7(this.getCasVyskytu());
            }
        }
    }
    
}
