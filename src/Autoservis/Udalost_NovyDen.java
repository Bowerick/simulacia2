/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Autoservis;

import udalostnaSimulacia.SimulacneJadro;

/**
 *
 * @author Robo
 */
public class Udalost_NovyDen extends AutoservisUdalost {

    private AutoservisSimulacneJadro jadroAutoservis;

    public Udalost_NovyDen(double casVyskytu, SimulacneJadro simJadro, AutoservisZakaznik zakaznik) {
        super(casVyskytu, simJadro, zakaznik);
        jadroAutoservis = (AutoservisSimulacneJadro) simJadro;
    }

    @Override
    public void execute() {
        /**
         *  atributy------------------------------------------------------------
         */
        int pocetLudiVRadePrichodovPredRestartom = jadroAutoservis.getDlzkaFrontuPrichodovZakaznikov();
        /**
         *  povinne udalosti----------------------------------------------------
         */
        double casDalsiehoDna = this.getCasVyskytu() + 8.0 * 60.0 * 60.0;
        jadroAutoservis.pridajUdalost(new Udalost_NovyDen(casDalsiehoDna, jadroAutoservis, null));
        jadroAutoservis.vynulujRadZakaznikovPriPrichode();
        
        /**
         *  volitelne udalosti--------------------------------------------------
         */
        
        /**
         *  statistika----------------------------------------------------------
         */
        
        if(jadroAutoservis.isZahriate()) {
            jadroAutoservis.getStatistika().pridajPocetLudiVRadePrichodov7(
                    (this.getCasVyskytu() - jadroAutoservis.getStatistika().getCasPoslednejZmenyRaduPrichodov7())*pocetLudiVRadePrichodovPredRestartom);
            jadroAutoservis.getStatistika().setCasPoslednejZmenyRaduPrichodov7(this.getCasVyskytu());
            jadroAutoservis.getStatistika().vynulujPocetLudiVRade1();
        }
    }
    
}
