/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Autoservis;

import udalostnaSimulacia.SimulacneJadro;

/**
 *
 * @author Robo
 */
public class Udalost04_ZaciatokPrevzatiaAuta extends AutoservisUdalost {

    private AutoservisSimulacneJadro jadroAutoservis;

    public Udalost04_ZaciatokPrevzatiaAuta(double casVyskytu, SimulacneJadro simJadro, AutoservisZakaznik zakaznik) {
        super(casVyskytu, simJadro, zakaznik);
        this.jadroAutoservis = (AutoservisSimulacneJadro) simJadro;
    }

    @Override
    public void execute() {
        /**
         *  atributy------------------------------------------------------------
         */
        
        
        /**
         *  povinne udalosti----------------------------------------------------
         */
        double casKoncaPrevzatiaAutaOdZakaznika = this.getCasVyskytu() + jadroAutoservis.dalsiCasDlzkyPrevzatiaAutaOdZakaznika();
        jadroAutoservis.pridajUdalost(new Udalost05_KoniecPrevzatiaAuta(casKoncaPrevzatiaAutaOdZakaznika, jadroAutoservis, this.getZakaznik()));
        /**
         *  volitelne udalosti--------------------------------------------------
         */
        
        /**
         *  statistika----------------------------------------------------------
         */
    }
    
}
