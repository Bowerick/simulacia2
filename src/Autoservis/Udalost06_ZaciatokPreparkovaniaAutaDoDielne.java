/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Autoservis;

import udalostnaSimulacia.SimulacneJadro;

/**
 *
 * @author Robo
 */
public class Udalost06_ZaciatokPreparkovaniaAutaDoDielne extends AutoservisUdalost {

    private AutoservisSimulacneJadro jadroAutoservis;

    public Udalost06_ZaciatokPreparkovaniaAutaDoDielne(double casVyskytu, SimulacneJadro simJadro, AutoservisZakaznik zakaznik) {
        super(casVyskytu, simJadro, zakaznik);
        this.jadroAutoservis = (AutoservisSimulacneJadro) simJadro;
    }

    @Override
    public void execute() {
        /**
         *  atributy------------------------------------------------------------
         */
        
        /**
         *  povinne udalosti----------------------------------------------------
         */
        double casKoncaPreparkovania = this.getCasVyskytu() + jadroAutoservis.dalsiCasDlzkyPreparkovaniaAuta();
        jadroAutoservis.pridajUdalost(new Udalost07_KoniecPreparkovaniaAutaDoDielne(casKoncaPreparkovania, jadroAutoservis, this.getZakaznik()));
        
        /**
         *  volitelne udalosti--------------------------------------------------
         */
        
        /**
         *  statistika----------------------------------------------------------
         */
    }
    
}
