/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Autoservis;

import java.awt.BorderLayout;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import udalostnaSimulacia.IDelegat;
import udalostnaSimulacia.SimulacneJadro;

/**
 *
 * @author Robo
 */
public class AppGui extends javax.swing.JFrame implements IDelegat {

    private App app;
    private App[] app2;
    private App[] app3;
    private App[] appExcel;
    private XYSeries series1;
    private XYSeries series2;
    private XYSeries series3;
    private XYSeries series4;
    private boolean graf34;
    /**
     * Creates new form AppGui
     */
    public AppGui() {
        initComponents();
        app = new App();
        app2 = new App[10];
        app3 = new App[11];
        appExcel = new App[7*22+1];
        graf34 = true;
        jSlider1.addChangeListener((e) -> {
            int value = jSlider1.getValue();
            //min 1sek - max 1 den
            double periodaAnimacie;
            /*if(value==0)
                periodaAnimacie = 1.0 + value*288.0; //v s
            else
                periodaAnimacie = value*288.0; //v s
            */
            if(value==0) {
                periodaAnimacie = 1.0; //v s
            }
            else {
                //periodaAnimacie = value*288.0; //v s
                periodaAnimacie = value*10.0; //v s
            }
            app.nastavPerioduAnimacie(periodaAnimacie);
            int den = (int) (periodaAnimacie/28800.0);
            periodaAnimacie = periodaAnimacie%28800.0;
            int hodina =(int) (periodaAnimacie/3600.0);
            int minuta =(int) ((periodaAnimacie%3600.0)/60.0);
            int sekunda =(int) (((periodaAnimacie%3600.0)%60.0));
            if(minuta<10 && sekunda<10)
                jTextField4.setText(den+"-"+"0"+hodina+":0"+minuta+":0"+sekunda+" (D-HH:MM:SS)");
            if(minuta<10 && sekunda>=10)
                jTextField4.setText(den+"-"+"0"+hodina+":0"+minuta+":"+sekunda+" (D-HH:MM:SS)");
            if(minuta>=10 && sekunda<10)
                jTextField4.setText(den+"-"+"0"+hodina+":"+minuta+":0"+sekunda+" (D-HH:MM:SS)");
            if(minuta>=10 && sekunda>=10)
                jTextField4.setText(den+"-"+"0"+hodina+":"+minuta+":"+sekunda+" (D-HH:MM:SS)");
        });
        
        XYSeriesCollection dataset = new XYSeriesCollection();
        series1 = new XYSeries("");
        dataset.addSeries(series1);
        JFreeChart chart = ChartFactory.createXYLineChart("Priemerný čas strávený zákazníkom čakaním na opravu", "Počet replikácií", "Čas", dataset, PlotOrientation.VERTICAL, true, true,false);
        
        XYPlot plot = chart.getXYPlot();
        NumberAxis x =(NumberAxis) plot.getRangeAxis();
        x.setAutoRangeIncludesZero(false);
        
        ChartPanel c1 = new ChartPanel(chart);
        jPanel5.removeAll();
        jPanel5.add(c1,BorderLayout.CENTER);
        jPanel5.validate();


        XYSeriesCollection dataset2 = new XYSeriesCollection();
        series2 = new XYSeries("");
        dataset2.addSeries(series2);
        JFreeChart chart2 = ChartFactory.createXYLineChart("Priemerný čas čakania v rade na zadanie objednávky", "Počet replikácií", "Čas", dataset2, PlotOrientation.VERTICAL, true, true,false);
        XYPlot plot2 = chart2.getXYPlot();
        NumberAxis x2 =(NumberAxis) plot2.getRangeAxis();
        x2.setAutoRangeIncludesZero(false);
        ChartPanel c2 = new ChartPanel(chart2);
        jPanel6.removeAll();
        jPanel6.add(c2,BorderLayout.CENTER);
        jPanel6.validate();

        XYSeriesCollection dataset3 = new XYSeriesCollection();
        series3 = new XYSeries("");
        dataset3.addSeries(series3);
        JFreeChart chart3 = ChartFactory.createXYLineChart("Závislosť priemerného počtu čakajúcich v rade na počte pracovníkov skupiny 1", "Počet pracovníkov skupiny 1", "Dĺžka radu", dataset3, PlotOrientation.VERTICAL, true, true,false);
        XYPlot plot3 = chart3.getXYPlot();
        NumberAxis x3 =(NumberAxis) plot3.getRangeAxis();
        x3.setAutoRangeIncludesZero(false);
        ChartPanel c3 = new ChartPanel(chart3);
        jPanel7.removeAll();
        jPanel7.add(c3,BorderLayout.CENTER);
        jPanel7.validate();


        XYSeriesCollection dataset4 = new XYSeriesCollection();
        series4 = new XYSeries("");
        dataset4.addSeries(series4);
        JFreeChart chart4 = ChartFactory.createXYLineChart("Závislosť priemerného času stráveného zákazníkom v servise na počte pracovníkov skupiny 2", "Počet pracovníkov skupiny 2", "Čas v servise", dataset4, PlotOrientation.VERTICAL, true, true,false);
        XYPlot plot4 = chart4.getXYPlot();
        NumberAxis x4 =(NumberAxis) plot4.getRangeAxis();
        x4.setAutoRangeIncludesZero(false);
        ChartPanel c4 = new ChartPanel(chart4);
        jPanel8.removeAll();
        jPanel8.add(c4,BorderLayout.CENTER);
        jPanel8.validate();

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jTextField2 = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jTextField3 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jProgressBar1 = new javax.swing.JProgressBar();
        jProgressBar2 = new javax.swing.JProgressBar();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jTextField5 = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jTextField6 = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jTextField7 = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jTextField8 = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jTextField9 = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jTextField10 = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jTextField11 = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jTextField12 = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jTextField15 = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        jTextField16 = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        jTextField13 = new javax.swing.JTextField();
        jTextField14 = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        jRadioButton1 = new javax.swing.JRadioButton();
        jRadioButton2 = new javax.swing.JRadioButton();
        jSlider1 = new javax.swing.JSlider();
        jTextField4 = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Počet pracovníkov typu 1");

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Počet pracovníkov typu 2");

        jTextField1.setText("5");

        jTextField2.setText("21");

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Počet replikácií");

        jTextField3.setText("1000");

        jButton1.setText("spust");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton5.setText("pozastav simuláciu");
        jButton5.setEnabled(false);
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        jButton6.setText("ukonči simuláciu");
        jButton6.setEnabled(false);
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jButton6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField1))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jTextField3, javax.swing.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE)
                            .addComponent(jTextField2)))
                    .addComponent(jButton1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton6)
                .addContainerGap(20, Short.MAX_VALUE))
        );

        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("Priemerná vyťaženost pracovníka typu 1");

        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("Priemerná vyťaženost pracovníka typu 2");

        jLabel6.setText("Simulačný čas");

        jTextField5.setText("0-00:00:00 (D-HH:MM:SS)");

        jLabel7.setText("Replikácia");

        jLabel8.setText("Rad zakazníkov");

        jTextField7.setColumns(1000);
        jTextField7.setMaximumSize(new java.awt.Dimension(192, 2147483647));

        jLabel9.setText("Objednávania");

        jTextField8.setColumns(1000);
        jTextField8.setMaximumSize(new java.awt.Dimension(192, 2147483647));

        jLabel10.setText("Rad aut na opravu");

        jTextField9.setColumns(1000);
        jTextField9.setMaximumSize(new java.awt.Dimension(192, 2147483647));

        jLabel11.setText("Opravovanie");

        jTextField10.setColumns(1000);
        jTextField10.setMaximumSize(new java.awt.Dimension(192, 2147483647));

        jLabel12.setText("Rad opravených aut");

        jTextField11.setColumns(1000);
        jTextField11.setMaximumSize(new java.awt.Dimension(192, 2147483647));

        jLabel13.setText("Odovzdanie auta");

        jTextField12.setColumns(1000);
        jTextField12.setMaximumSize(new java.awt.Dimension(192, 2147483647));

        jLabel14.setText("Voľní prac. 1");

        jTextField15.setColumns(1000);
        jTextField15.setMaximumSize(new java.awt.Dimension(192, 2147483647));

        jLabel15.setText("Voľní prac. 2");

        jTextField16.setColumns(1000);
        jTextField16.setMaximumSize(new java.awt.Dimension(192, 2147483647));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jProgressBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jProgressBar2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jTextField5, javax.swing.GroupLayout.DEFAULT_SIZE, 179, Short.MAX_VALUE)
                            .addComponent(jTextField6))
                        .addGap(0, 74, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, 113, Short.MAX_VALUE)
                                .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField11, javax.swing.GroupLayout.DEFAULT_SIZE, 212, Short.MAX_VALUE)
                            .addComponent(jTextField10, javax.swing.GroupLayout.DEFAULT_SIZE, 212, Short.MAX_VALUE)
                            .addComponent(jTextField9, javax.swing.GroupLayout.DEFAULT_SIZE, 212, Short.MAX_VALUE)
                            .addComponent(jTextField8, javax.swing.GroupLayout.DEFAULT_SIZE, 212, Short.MAX_VALUE)
                            .addComponent(jTextField7, javax.swing.GroupLayout.DEFAULT_SIZE, 212, Short.MAX_VALUE)
                            .addComponent(jTextField12, javax.swing.GroupLayout.DEFAULT_SIZE, 212, Short.MAX_VALUE)
                            .addComponent(jTextField16, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addComponent(jTextField15, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7)
                    .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jProgressBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jProgressBar2, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jTextField15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jTextField16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jTextField7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jTextField8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jTextField9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jTextField10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jTextField11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jTextField12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel5.setLayout(new java.awt.BorderLayout());

        jPanel6.setLayout(new java.awt.BorderLayout());

        jPanel7.setPreferredSize(new java.awt.Dimension(0, 0));
        jPanel7.setLayout(new java.awt.BorderLayout());

        jPanel8.setPreferredSize(new java.awt.Dimension(0, 0));
        jPanel8.setLayout(new java.awt.BorderLayout());

        jTextField13.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTextField13.setText("Interval spoľahlivosti: ");

        jTextField14.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTextField14.setText("Interval spoľahlivosti: ");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jTextField13)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, 387, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, 424, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(jTextField14, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextField13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextField14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, 363, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, 363, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(19, Short.MAX_VALUE))
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        buttonGroup1.add(jRadioButton1);
        jRadioButton1.setText("Rýchly režim");
        jRadioButton1.setEnabled(false);
        jRadioButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton1ActionPerformed(evt);
            }
        });

        buttonGroup1.add(jRadioButton2);
        jRadioButton2.setSelected(true);
        jRadioButton2.setText("Animačný režim");
        jRadioButton2.setEnabled(false);
        jRadioButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton2ActionPerformed(evt);
            }
        });

        jSlider1.setMaximum(259200);
        jSlider1.setValue(0);
        jSlider1.setEnabled(false);

        jTextField4.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTextField4.setText("0-00:00:01 (D-HH:MM:SS)");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jRadioButton2, javax.swing.GroupLayout.DEFAULT_SIZE, 131, Short.MAX_VALUE)
                            .addComponent(jRadioButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSlider1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jTextField4)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jRadioButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jRadioButton2)
                    .addComponent(jSlider1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(23, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        String prac1 = jTextField1.getText();
        String prac2 = jTextField2.getText();
        String pocetRepl = jTextField3.getText();
        try {
            int pocetPracvTypu1 = Integer.parseInt(prac1);
            int pocetPracvTypu2 = Integer.parseInt(prac2);
            int pocetReplikacii = Integer.parseInt(pocetRepl);
            app.setPocetPracovnikovTypu1(pocetPracvTypu1);
            app.setPocetPracovnikovTypu2(pocetPracvTypu2);
            app.setPocetReplikacii(pocetReplikacii);
            app.setDelegat(this);
                                     
            series1.clear();
            series2.clear();    
            //series3.clear();
            //series4.clear();
            app.setZavislosti(false);
            app.setZavislosti2(false);
            app.setAnimacia(true);
            app.setExcel(false);
            new Thread(app).start();
            
            int value = jSlider1.getValue();
            double periodaAnimacie;
            if(value==0) {
                periodaAnimacie = 1.0; //v s
            }
            else {
                //periodaAnimacie = value*288.0; //v s
                periodaAnimacie = value*10.0; //v s
            }
            app.setAnimacnaPerioda(periodaAnimacie);
            
            jButton5.setEnabled(true);
            jButton6.setEnabled(true);
            jSlider1.setEnabled(true);
            jRadioButton1.setEnabled(true);
            //jRadioButton1.setSelected(true);
            jRadioButton2.setEnabled(true);
            if(graf34) {
                Executor executor = Executors.newSingleThreadExecutor();
                for(int i=0;i<10;i++) {
                    app2[i] = new App();
                    app2[i].setPocetPracovnikovTypu1((i+1));
                    app2[i].setPocetPracovnikovTypu2(21);
                    app2[i].setPocetReplikacii(10);
                    app2[i].setZavislosti(true);
                    app2[i].setZavislosti2(false);
                    app2[i].setDelegat(this);
                    app2[i].setExcel(false);
                    app2[i].setAnimacia(false);
                    executor.execute(app2[i]);
                }
                Executor executor2 = Executors.newSingleThreadExecutor();
                for(int i=0;i<11;i++) {
                    app3[i] = new App();
                    app3[i].setPocetPracovnikovTypu1(5);
                    app3[i].setAnimacia(false);
                    app3[i].setPocetPracovnikovTypu2((15+i+1));
                    app3[i].setPocetReplikacii(10);
                    app3[i].setZavislosti(false);
                    app3[i].setZavislosti2(true);
                    app3[i].setDelegat(this);
                    app3[i].setExcel(false);
                    executor2.execute(app3[i]);
                }
            }
            /*
            Executor executor3 = Executors.newSingleThreadExecutor();
            for(int i=1;i<=7;i++) { 
                for(int j=1;j<=22;j++) {
                appExcel[i*j] = new App();
                appExcel[i*j].setPocetPracovnikovTypu1(i);
                appExcel[i*j].setPocetPracovnikovTypu2(j);
                appExcel[i*j].setPocetReplikacii(10);
                appExcel[i*j].setZavislosti(false);
                appExcel[i*j].setZavislosti(false);
                appExcel[i*j].setDelegat(this);
                appExcel[i*j].setExcel(true);
                executor3.execute(appExcel[i*j]);
                }
            }
            */
            /*
            Thread t;
            for(int i=0;i<10;i++) {
                app2.setPocetPracovnikovTypu1((i+1));
                app2.setPocetPracovnikovTypu2(21);
                app2.setPocetReplikacii(100);
                app2.setDelegat(this);
                t = new Thread(app2);
                t.start();
                try {
                    t.join();
                } catch (InterruptedException ex) {
                    Logger.getLogger(AppGui.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            */
            /*
            Runnable r = new Runnable() {
                @Override
                public void run() {
                    for(int i=0;i<10;i++) {
                        app2.setPocetPracovnikovTypu1((i+1));
                        app2.setPocetPracovnikovTypu2(21);
                        app2.setPocetReplikacii(100);
                        //app2.setDelegat(this);
                        Thread t = new Thread(app2);
                        t.start();
                        System.out.println("...:: "+i);
                    }
                }
            };
            new Thread(r).start();
            */
            graf34 = false;
        } catch(NumberFormatException ex) {
            ex.printStackTrace();
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        if(app.getServis().isPauza()) {
            jButton5.setText("zastav simuláciu");
            app.pokracuj();
        }
        else {
            jButton5.setText("pokračuj v simulácií");
            app.pozastav();
        }
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        app.ukonci();
        jButton5.setEnabled(false);
        jButton6.setEnabled(false);
           
            jTextField7.setText("");
            jTextField8.setText("");
            jTextField9.setText("");
            jTextField10.setText("");
            jTextField11.setText("");
            jTextField12.setText("");
            jTextField15.setText("");
            jTextField16.setText("");
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jRadioButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton1ActionPerformed
        // Rychly
        if(jRadioButton1.isSelected()) {
            app.zastavAnimaciu();
        }
        
    }//GEN-LAST:event_jRadioButton1ActionPerformed

    private void jRadioButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton2ActionPerformed
        // Animacny
        if(jRadioButton2.isSelected()) {
            int value = jSlider1.getValue();
            //min 1sek - max 1 den
            //double periodaAnimacie = 1000.0 + value*288.0;
            double periodaAnimacie; //v s
            if(value==0) {
                periodaAnimacie = 1.0; //v s
            }
            else {
                //periodaAnimacie = value*288.0; //v s
                periodaAnimacie = value*10.0; //v s
            }
            System.out.println("SPUSTAM");
            app.spustAnimaciu(periodaAnimacie);
        }
    }//GEN-LAST:event_jRadioButton2ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AppGui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AppGui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AppGui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AppGui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new AppGui().setVisible(true);
            }
        });
    }

    @Override
    public void obnovGUI(SimulacneJadro sim) {
        //toDo ...
        if(app.getServis().isAnimacia()) {
            double progress1 =  ((AutoservisSimulacneJadro)sim).getStatistika().getSumaPriemerovPoctuVolnychPracovnikov1()/((double)app.getServis().getStatistika().getAktualnaReplikacia());
            progress1 /= (double)((AutoservisSimulacneJadro)sim).getPocetPracovnikovTypu1();
            progress1 = 1.0 - progress1;
            jProgressBar1.setValue((int)(progress1*100.0));
            progress1 = Math.round(progress1*1000000.0)/10000.0;
            jProgressBar1.setString(progress1+" %");
            jProgressBar1.setStringPainted(true);
            
            double progress2 = ((AutoservisSimulacneJadro)sim).getStatistika().getSumaPriemerovPoctuVolnychPracovnikov2()/((double)app.getServis().getStatistika().getAktualnaReplikacia());
            progress2 /= (double)((AutoservisSimulacneJadro)sim).getPocetPracovnikovTypu2();
            progress2 = 1.0 - progress2;
            jProgressBar2.setValue((int)(progress2*100.0));
            progress2 = Math.round(progress2*1000000.0)/10000.0;
            jProgressBar2.setString(progress2+" %");
            jProgressBar2.setStringPainted(true);
            //-----------------------------------------------------
            jTextField6.setText(((AutoservisSimulacneJadro)sim).getStatistika().getAktualnaReplikacia()+"");
            double cas = ((AutoservisSimulacneJadro)sim).getSimulacnyCas();
            int den = (int) (cas/(8.0*60.0*60.0));
            int hodina =(int) ((cas%(8.0*60.0*60.0))/3600.0);
            int minuta =(int) (((cas%(8.0*60.0*60.0))%3600.0)/60.0);
            int sekunda =(int) (((((cas%(8.0*60.0*60.0))%3600.0)%60.0)%60.0));
            System.out.println(""+den+"-"+hodina+":"+minuta+":"+sekunda);
            
            if(minuta<10 && sekunda<10)
                jTextField5.setText(den+"-0"+hodina+":0"+minuta+":0"+sekunda+" (D-HH:MM:SS)");
            if(minuta<10 && sekunda>=10)
                jTextField5.setText(den+"-0"+hodina+":0"+minuta+":"+sekunda+" (D-HH:MM:SS)");
            if(minuta>=10 && sekunda<10)
                jTextField5.setText(den+"-0"+hodina+":"+minuta+":0"+sekunda+" (D-HH:MM:SS)");
            if(minuta>=10 && sekunda>=10)
                jTextField5.setText(den+"-0"+hodina+":"+minuta+":"+sekunda+" (D-HH:MM:SS)");
            
            jTextField7.setText(""+((AutoservisSimulacneJadro)sim).getDlzkaFrontuPrichodovZakaznikov());
            jTextField8.setText(((AutoservisSimulacneJadro)sim).getStatistika().getPracovniciNaObjednavani());
            jTextField9.setText(""+((AutoservisSimulacneJadro)sim).getDlzkaFrontuAutCakajucichNaOpravu());
            jTextField10.setText(((AutoservisSimulacneJadro)sim).getStatistika().getPracovniciNaOprave());
            jTextField11.setText(""+((AutoservisSimulacneJadro)sim).getDlzkaFrontuAutCakajucichNaPreparkovanie());
            jTextField12.setText(((AutoservisSimulacneJadro)sim).getStatistika().getPracovniciNaPreparkovaniPlusOdovzdani());
            jTextField15.setText(((AutoservisSimulacneJadro)sim).getVolniPracovnic1());
            jTextField16.setText(((AutoservisSimulacneJadro)sim).getVolniPracovnici2());
            
        }
        
    }
    
    @Override
    public void obnovGraf(SimulacneJadro sim) {
        if(!((AutoservisSimulacneJadro)sim).isZavislosti() && !((AutoservisSimulacneJadro)sim).isZavislosti2()) {
            //System.out.println((((AutoservisSimulacneJadro)sim).getStatistika().getAktualnaReplikacia() % ((((AutoservisSimulacneJadro)sim).getPocetReplikacii()/1000)+1))+",   "+((AutoservisSimulacneJadro)sim).getStatistika().getAktualnaReplikacia());
            if((((AutoservisSimulacneJadro)sim).getStatistika().getAktualnaReplikacia() % ((((AutoservisSimulacneJadro)sim).getPocetReplikacii()/1000)+1)) == 0 
                && (((AutoservisSimulacneJadro)sim).getStatistika().getAktualnaReplikacia() >= (((AutoservisSimulacneJadro)sim).getPocetReplikacii()*0.40) ||((AutoservisSimulacneJadro)sim).getStatistika().getAktualnaReplikacia()>1000)
                    && !((AutoservisSimulacneJadro)sim).isExcel()) {
                series1.add(((AutoservisSimulacneJadro)sim).getStatistika().getAktualnaReplikacia(), ((AutoservisSimulacneJadro)sim).getStatistika().getStatistika1());
                series2.add(((AutoservisSimulacneJadro)sim).getStatistika().getAktualnaReplikacia(), ((AutoservisSimulacneJadro)sim).getStatistika().getStatistika2());
                double[] is = ((AutoservisSimulacneJadro)sim).getStatistika().getIs1();
                double[] is2 = ((AutoservisSimulacneJadro)sim).getStatistika().getIs2();
                jTextField13.setText("Interval spoľahlivosti:  <"+is[0]+", "+is[1]+">");
                jTextField14.setText("Interval spoľahlivosti:  <"+is2[0]+", "+is2[1]+">");
                System.out.println(((AutoservisSimulacneJadro)sim).getStatistika().getStatistika1());
                System.out.println(((AutoservisSimulacneJadro)sim).getStatistika().getStatistika1111());
                System.out.println("_______________________________________");
            }
            /*
            if(((AutoservisSimulacneJadro)sim).isExcel()) {
                if(((AutoservisSimulacneJadro)sim).getStatistika().getAktualnaReplikacia()  == ((AutoservisSimulacneJadro)sim).getPocetReplikacii()) {
                    double[] is = ((AutoservisSimulacneJadro)sim).getStatistika().getIs1();
                    double[] is2 = ((AutoservisSimulacneJadro)sim).getStatistika().getIs2();
                    System.out.println(((AutoservisSimulacneJadro)sim).getStatistika().getStatistika1()+
                            "| "+((AutoservisSimulacneJadro)sim).getStatistika().getStatistika2()+
                            "| "+is[0]+
                            "| "+is[1]);
                }
            }
            */
        }
        
    }
    
    @Override
    public void obnovGrafZavislosti(SimulacneJadro sim) {
        series3.add(((AutoservisSimulacneJadro)sim).getPocetPracovnikovTypu1(), ((AutoservisSimulacneJadro)sim).getStatistika().getStatistika7());
    }

    @Override
    public void obnovGrafZavislosti2(SimulacneJadro sim) {
        series4.add(((AutoservisSimulacneJadro)sim).getPocetPracovnikovTypu2(), ((AutoservisSimulacneJadro)sim).getStatistika().getStatistika4());
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JProgressBar jProgressBar1;
    private javax.swing.JProgressBar jProgressBar2;
    private javax.swing.JRadioButton jRadioButton1;
    private javax.swing.JRadioButton jRadioButton2;
    private javax.swing.JSlider jSlider1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField10;
    private javax.swing.JTextField jTextField11;
    private javax.swing.JTextField jTextField12;
    private javax.swing.JTextField jTextField13;
    private javax.swing.JTextField jTextField14;
    private javax.swing.JTextField jTextField15;
    private javax.swing.JTextField jTextField16;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField4;
    private javax.swing.JTextField jTextField5;
    private javax.swing.JTextField jTextField6;
    private javax.swing.JTextField jTextField7;
    private javax.swing.JTextField jTextField8;
    private javax.swing.JTextField jTextField9;
    // End of variables declaration//GEN-END:variables


}
