/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generator;

import java.util.Random;

/**
 *
 * @author Robo
 */
public class Trojuholnikove {
    private Random generator;
    private double min;
    private double max;
    private double modus;
    
    public Trojuholnikove(long seed, double min, double max, double modus) {
        this.min = min;
        this.max = max;
        this.modus = modus;
        this.generator = new Random(seed);
    }
    
    public double nextDouble() {
        double gen = generator.nextDouble();
        if(gen < (modus-min)/(max-min)) {
            return min + Math.sqrt(gen*(max-min)*(modus-min));
        } else {
            return max - Math.sqrt((1.0-gen)*(max-min)*(max-modus));
        }
    }
}
