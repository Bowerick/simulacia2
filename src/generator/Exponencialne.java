/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generator;

import java.util.Random;

/**
 *
 * @author Robo
 */
public class Exponencialne {
    
    private Random generator;
    private double lambda;
    
    /**
     *
     * @param lambda - stredna hodnota v minutach
     */
    public Exponencialne(long nasada,double lambda) {
        this.generator = new Random(nasada);
        this.lambda = 1.0 /(lambda);
    }
    
    public double nextDouble() {
        return ((-1.0* Math.log(1.0 - generator.nextDouble())) / (lambda));
    }
    
}
