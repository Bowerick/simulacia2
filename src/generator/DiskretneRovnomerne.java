/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generator;

import java.util.Random;

/**
 *
 * @author Robo
 */
public class DiskretneRovnomerne {
    private Random generator;
    private int min;
    private int max;
    
    public DiskretneRovnomerne(long nasada, int min, int max) {
        this.generator = new Random(nasada);
        this.min = min;
        this.max = max + 1;
    }
    
    public int nextInt() {
        return generator.nextInt(max - min) +min;
    }
}
