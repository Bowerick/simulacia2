/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generator;

import java.util.Random;

/**
 *
 * @author Robo
 */
public class EmpirickeDiskretne {
    private Random generator;
    private double[] pravdepodonosti;
    
    public EmpirickeDiskretne(long nasada, double[] pravdepodobnosti) throws Exception {
        this.generator = new Random(nasada);
        this.pravdepodonosti = pravdepodobnosti;
        double kontrola = 0.0;
        for(int i=0;i<pravdepodonosti.length;i++) 
            kontrola += pravdepodobnosti[i];
        if(Math.abs(kontrola - 1.0) > 0.0000001) {
            System.out.println("?????????????????????");
            throw new Exception("Suma pravdepodobnosti musi byt 1.0 ");
        }
    }
    /*
    public int nextInt() {
        double gen = generator.nextDouble();
        for(int i=0;i<pravdepodonosti.length;i++) {
            if(gen < pravdepodonosti[i]) {
                return i+1;
            } 
        }
        return -1;
    }
    */
    public int nextInt() {
        double gen = generator.nextDouble();
        double suma = pravdepodonosti[0];
        for(int i=0;i<pravdepodonosti.length;i++) {
            if(gen < suma) {
                return (i+1);
            } else {
                suma += pravdepodonosti[i+1];
            }
        }
        
        for(int i=0;i<pravdepodonosti.length;i++) {
            System.out.print(pravdepodonosti[i]+", ");
        }
        System.out.println();
        double suma2 = pravdepodonosti[0];
        System.out.print(suma2+", ");
        for(int i=0;i<pravdepodonosti.length;i++) {
            if(gen < suma2) {
                System.out.print(suma2+", -mam"+(i+1));
                System.out.println();
            } else {
                suma2 += pravdepodonosti[i+1];
                System.out.print(suma2+", ");
            }
        }
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!! "+gen);
        return -1;
    }
    
}
