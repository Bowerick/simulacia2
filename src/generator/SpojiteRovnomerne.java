/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generator;

import java.util.Random;

/**
 *
 * @author Robo
 */
public class SpojiteRovnomerne {
    private Random generator;
    private double min;
    private double max;
    
    public SpojiteRovnomerne(long seed, double min, double max) {
        this.generator = new Random(seed);
        this.min = min;
        this.max = max;
    }
    
    public double nextDouble() {
        return (generator.nextDouble() * (max-min)) + min;
    }
}
