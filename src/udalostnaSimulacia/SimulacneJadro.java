/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package udalostnaSimulacia;
import java.util.PriorityQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Robo
 */
public abstract class SimulacneJadro {
    
    private double simulacnyCas;
    private boolean excel;
    private PriorityQueue<Udalost> casovaOs;
    private int pocetReplikacii;
    private Udalost startovaciaUdalost;
    private IDelegat delegat;
    private boolean animacia;
    private boolean zacniAnimaciu;
    private boolean pauza;
    private boolean koniec;
    private boolean zavislosti;
    private boolean zavislosti2;
    
    public SimulacneJadro(int pocetReplikacii) {
        this.casovaOs = new PriorityQueue<>();
        this.pocetReplikacii = pocetReplikacii;
        this.koniec = false;
    }
    
    public void simuluj(double maxCas) {
        for(int i=0;i<pocetReplikacii;i++) {
            
            predReplikaciou();
            while(maxCas >= getSimulacnyCas() && !casovaOs.isEmpty()) {
                if(koniec) {
                    break;
                }
                while(isPauza()) {
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(SimulacneJadro.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                if(isZacniAnimaciu()) {
                    zacniAnimaciu();
                }
                Udalost u = getCasovaOs().poll();
                setSimulacnyCas(u.getCasVyskytu());
                u.execute();
                //System.out.println(u.toString());
            }
            if(koniec) {
                break;
            }
            poReplikacii();
            delegat.obnovGraf(this);
        }
        if(zavislosti) {
            delegat.obnovGrafZavislosti(this);
        }
        if(zavislosti2) {
            delegat.obnovGrafZavislosti2(this);
        }
    }

    public abstract void predReplikaciou();

    public abstract void poReplikacii();

    public abstract void zacniAnimaciu();
    
    public boolean pridajUdalost(Udalost u) {
        return this.getCasovaOs().add(u);
    }
    
    public void obnovGui(SimulacneJadro sim) {
        delegat.obnovGUI(sim);
    }

    /**
     * @return the simulacnyCas
     */
    public double getSimulacnyCas() {
        return simulacnyCas;
    }

    /**
     * @return the startovaciaUdalost
     */
    public Udalost getStartovaciaUdalost() {
        return startovaciaUdalost;
    }

    /**
     * @param startovaciaUdalost the startovaciaUdalost to set
     */
    public void setStartovaciaUdalost(Udalost startovaciaUdalost) {
        this.startovaciaUdalost = startovaciaUdalost;
    }

    /**
     * @param simulacnyCas the simulacnyCas to set
     */
    public void setSimulacnyCas(double simulacnyCas) {
        this.simulacnyCas = simulacnyCas;
    }

    /**
     * @return the animacia
     */
    public boolean isAnimacia() {
        return animacia;
    }

    /**
     * @param animacia the animacia to set
     */
    public void setAnimacia(boolean animacia) {
        this.animacia = animacia;
    }

    /**
     * @return the zacniAnimaciu
     */
    public boolean isZacniAnimaciu() {
        return zacniAnimaciu;
    }

    /**
     * @param zacniAnimaciu the zacniAnimaciu to set
     */
    public void setZacniAnimaciu(boolean zacniAnimaciu) {
        this.zacniAnimaciu = zacniAnimaciu;
    }

    /**
     * @param delegat the delegat to set
     */
    public void setDelegat(IDelegat delegat) {
        this.delegat = delegat;
    }

    /**
     * @param pauza the pauza to set
     */
    public void setPauza(boolean pauza) {
        this.pauza = pauza;
    }

    /**
     * @param casovaOs the casovaOs to set
     */
    public void setCasovaOs(PriorityQueue<Udalost> casovaOs) {
        this.casovaOs = casovaOs;
    }

    /**
     * @return the casovaOs
     */
    public PriorityQueue<Udalost> getCasovaOs() {
        return casovaOs;
    }
    
    public void vynulujCasovuOs() {
        this.casovaOs.clear();
    }

    /**
     * @return the pauza
     */
    public boolean isPauza() {
        return pauza;
    }

    /**
     * @return the koniec
     */
    public boolean isKoniec() {
        return koniec;
    }

    /**
     * @param koniec the koniec to set
     */
    public void setKoniec(boolean koniec) {
        this.koniec = koniec;
    }

    /**
     * @return the pocetReplikacii
     */
    public int getPocetReplikacii() {
        return pocetReplikacii;
    }

    /**
     * @return the zavislosti
     */
    public boolean isZavislosti() {
        return zavislosti;
    }

    /**
     * @param zavislosti the zavislosti to set
     */
    public void setZavislosti(boolean zavislosti) {
        this.zavislosti = zavislosti;
    }

    /**
     * @param zavislosti2 the zavislosti2 to set
     */
    public void setZavislosti2(boolean zavislosti2) {
        this.zavislosti2 = zavislosti2;
    }

    /**
     * @return the zavislosti2
     */
    public boolean isZavislosti2() {
        return zavislosti2;
    }
    
    public boolean isExcel() {
        return this.excel;
    }
    public void setExcel(boolean e) {
        this.excel = e;
    }
}
