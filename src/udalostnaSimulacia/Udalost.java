/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package udalostnaSimulacia;

/**
 *
 * @author Robo
 */
public abstract class Udalost implements Comparable<Udalost>{
    
    private double casVyskytu;
    private SimulacneJadro simJadro;
    
    public Udalost(double casVyskytu, SimulacneJadro simJadro) {
        this.casVyskytu = casVyskytu;
        this.simJadro = simJadro;
    }
    
    public abstract void execute();

    @Override
    public int compareTo(Udalost u){
        // compareTo should return < 0 if this is supposed to be
        // less than other, > 0 if this is supposed to be greater than 
        // other and 0 if they are supposed to be equal
        if(this.getCasVyskytu() < u.getCasVyskytu()) {
            return -1;
        }
        if(this.getCasVyskytu() > u.getCasVyskytu()) {
            return 1;
        }
        return 0;
    }
    
    /**
     * @return the casVyskytu
     */
    public double getCasVyskytu() {
        return casVyskytu;
    }

    /**
     * @param casVyskytu the casVyskytu to set
     */
    public void setCasVyskytu(double casVyskytu) {
        this.casVyskytu = casVyskytu;
    }

    /**
     * @return the simJadro
     */
    public SimulacneJadro getSimJadro() {
        return simJadro;
    }

    /**
     * @param simJadro the simJadro to set
     */
    public void setSimJadro(SimulacneJadro simJadro) {
        this.simJadro = simJadro;
    }
    
}
