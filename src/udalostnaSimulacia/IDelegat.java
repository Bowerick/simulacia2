/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package udalostnaSimulacia;

/**
 *
 * @author Robo
 */
public interface IDelegat {
    public void obnovGUI(SimulacneJadro sim);
    public void obnovGraf(SimulacneJadro sim);
    public void obnovGrafZavislosti(SimulacneJadro sim);
    public void obnovGrafZavislosti2(SimulacneJadro sim);
}
